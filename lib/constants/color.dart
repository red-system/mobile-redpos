import 'package:flutter/material.dart';

final cBorder = Colors.grey[300];

const cTransparant = Colors.transparent;

const cBlack = Color(0xFF000000);
const cBlack95 = Color(0xFF0d0d0d);
const cBlack90 = Color(0xFF1a1a1a);
const cBlack85 = Color(0xFF262626);
const cBlack80 = Color(0xFF333333);
const cBlack75 = Color(0xFF404040);
const cBlack70 = Color(0xFF4d4d4d);
const cBlack65 = Color(0xFF595959);
const cBlack60 = Color(0xFF666666);
const cBlack55 = Color(0xFF737373);
const cBlack50 = Color(0xFF808080);
const cBlack45 = Color(0xFF8c8c8c);
const cBlack40 = Color(0xFF999999);
const cBlack35 = Color(0xFFa6a6a6);
const cBlack30 = Color(0xFFb3b3b3);
const cBlack25 = Color(0xFFbfbfbf);
const cBlack20 = Color(0xFFcccccc);
const cBlack15 = Color(0xFFd9d9d9);
const cBlack10 = Color(0xFFe6e6e6);
const cBlack05 = Color(0xFFf2f2f2);
const cWhite = Color(0xFFFFFFFF);

const cBlue = Color(0xFF0039e6);
const cBlue55 = Color(0xFF0033cc);
const cBlue50 = Color(0xFF0040ff);
const cBlue45 = Color(0xFF1a53ff);
const cBlue40 = Color(0xFF3366ff);
const cBlue35 = Color(0xFF4d79ff);
const cBlue30 = Color(0xFF668cff);
const cBlue25 = Color(0xFF809fff);
const cBlue20 = Color(0xFF99b3ff);
const cBlue15 = Color(0xFFb3c6ff);
const cBlue10 = Color(0xFFccd9ff);
const cBlue05 = Color(0xFFe6ecff);

const cIconMenu = Color(0xFF0076ff);

const cRed = Color(0xFFb30000);
const cRed60 = Color(0xFFcc0000);
const cRed55 = Color(0xFFe60000);
const cRed50 = Color(0xFFff0000);
const cRed45 = Color(0xFFff1a1a);
const cRed40 = Color(0xFFff3333);
const cRed35 = Color(0xFFff4d4d);
const cRed30 = Color(0xFFff6666);
const cRed25 = Color(0xFFff8080);
const cRed20 = Color(0xFFff9999);
const cRed15 = Color(0xFFffb3b3);
const cRed10 = Color(0xFFffcccc);
const cRed05 = Color(0xFFffe6e6);

final Map<int, Color> cPrimarySwatch = {
  50: cBlue15,
  100: cBlue20,
  200: cBlue25,
  300: cBlue30,
  400: cBlue35,
  500: cBlue40,
  600: cBlue45,
  700: cBlue50,
  800: cBlue55,
  900: cBlue,
};

Color cGrey10 = Colors.grey[100];
Color cGrey20 = Colors.grey[200];
Color cGrey30 = Colors.grey[300];
Color cGrey40 = Colors.grey[400];
Color cGrey = Colors.grey[500];
Color cGrey60 = Colors.grey[600];
Color cGrey70 = Colors.grey[700];
Color cGrey80 = Colors.grey[800];
Color cGrey85 = Colors.grey[850];
Color cGrey90 = Colors.grey[900];

Color cBlueGrey10 = Colors.blueGrey[100];
Color cBlueGrey20 = Colors.blueGrey[200];
Color cBlueGrey30 = Colors.blueGrey[300];
Color cBlueGrey40 = Colors.blueGrey[400];
Color cBlueGrey = Colors.blueGrey[500];
Color cBlueGrey60 = Colors.blueGrey[600];
Color cBlueGrey70 = Colors.blueGrey[700];
Color cBlueGrey80 = Colors.blueGrey[800];
Color cBlueGrey90 = Colors.blueGrey[900];

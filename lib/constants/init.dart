import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:line_icons/line_icons.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:intl/intl.dart';
import 'package:redpos/core/models/customer_response.dart';
import 'package:redpos/core/models/produk_response.dart';
import 'package:redpos/core/models/transaksi_response.dart';

final sess = GetStorage();

final money =
    new NumberFormat.simpleCurrency(locale: 'id', name: '', decimalDigits: 0);
final datetime = DateFormat('yyyy-MM-dd kk:mm:ss');

/* SIGNINSIGNOUT */
bool hiddenPassword = true;

/* HOME */
List<Map<String, dynamic>> listHome = [
  {'nama': 'Pos', 'img': 'shopping_cart.png'},
  {'nama': 'Listing', 'img': 'listing.png'},
  // {'nama': 'Profil', 'img': 'profil-avatar.png'},
  {'nama': 'Log Out', 'img': 'signout.png'},
];

/* POS */

List<CustomerResponse> customerList = [];
List<CustomerResponse> customerListSearch = [];
List<ProdukResponse> produkList = [];
List<ProdukResponse> produkListSearch = [];
List<CartResponse> cartList = [];
List<TransaksiResponse> transaksiList = [];
List pembayaranList = [
  'Tunai',
  'Visa',
  'Master Card',
  'American Expres (Amex)',
  'Debet',
  'Cek',
  'Bilyet Giro (BG)',
  'Piutang'
];

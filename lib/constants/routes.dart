import 'package:flutter/material.dart';

routePush(context, destination){
  return Navigator.push(context, new MaterialPageRoute(builder: (context) => destination));
}

routePushReplacement(context, destination){
  return Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => destination));
}

routePop(context){
  return Navigator.pop(context);
}

routePopWithParam(context, result){
  return Navigator.pop(context, result);
}

routePopUntil(context, counter){
  var count = 0;
  return Navigator.popUntil(context, (route) => count++ == counter);
}

focusScope(context){
  return FocusScope.of(context).unfocus();
}

import 'package:flutter/material.dart';


const double kSizeBottomBar     = 82.0;
const double kSizeTopBar        = 106.0;
const double kSizeTopBottomBar  = kSizeTopBar+kSizeBottomBar;
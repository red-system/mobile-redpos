import 'package:flutter/material.dart';

double screenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

double screenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

screenPadding(BuildContext context) {
  return MediaQuery.of(context).padding;
}


class Size {
  static MediaQueryData mediaQueryData;
  static double screenWidth;
  static double screenHeight;

  void init (BuildContext context){
    mediaQueryData  = MediaQuery.of(context);
    screenWidth     = mediaQueryData.size.width;
    screenHeight    = mediaQueryData.size.height;
  }
}
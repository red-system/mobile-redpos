import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';

final sAlignmentTL = Alignment.topLeft;
final sAlignmentTR = Alignment.topRight;
final sAlignmentTC = Alignment.topRight;

final sAlignmentBL = Alignment.bottomLeft;
final sAlignmentBR = Alignment.bottomRight;
final sAlignmentBC = Alignment.bottomRight;

final sAlignmentCL = Alignment.centerLeft;
final sAlignmentCR = Alignment.centerRight;
final sAlignmentC = Alignment.center;

final sTextAlignR = TextAlign.right;
final sTextAlignC = TextAlign.center;

final sSizedBoxH5 = SizedBox(height: 5);
final sSizedBoxH10 = SizedBox(height: 10);
final sSizedBoxH15 = SizedBox(height: 15);
final sSizedBoxH20 = SizedBox(height: 20);
final sSizedBoxH25 = SizedBox(height: 25);
final sSizedBoxH30 = SizedBox(height: 30);
final sSizedBoxH35 = SizedBox(height: 35);
final sSizedBoxH40 = SizedBox(height: 40);

final sSizedBoxW5 = SizedBox(width: 5);
final sSizedBoxW10 = SizedBox(width: 10);
final sSizedBoxW15 = SizedBox(width: 15);
final sSizedBoxW20 = SizedBox(width: 20);
final sSizedBoxW25 = SizedBox(width: 25);
final sSizedBoxW30 = SizedBox(width: 30);
final sSizedBoxW35 = SizedBox(width: 35);
final sSizedBoxW40 = SizedBox(width: 40);

final sNoPadding = EdgeInsets.zero;

final sPaddingAll5 = EdgeInsets.all(5);
final sPaddingAll10 = EdgeInsets.all(10);
final sPaddingAll15 = EdgeInsets.all(15);
final sPaddingAll20 = EdgeInsets.all(20);

final sPaddingHor5 = EdgeInsets.symmetric(horizontal: 5);
final sPaddingHor10 = EdgeInsets.symmetric(horizontal: 10);
final sPaddingHor15 = EdgeInsets.symmetric(horizontal: 15);
final sPaddingHor20 = EdgeInsets.symmetric(horizontal: 20);

final sPaddingVer5 = EdgeInsets.symmetric(vertical: 5);
final sPaddingVer10 = EdgeInsets.symmetric(vertical: 10);
final sPaddingVer15 = EdgeInsets.symmetric(vertical: 15);
final sPaddingVer20 = EdgeInsets.symmetric(vertical: 20);

final sBorder = Border.all(color: cBorder);
final sBorderRadius1 = BorderRadius.circular(1);
final sBorderRadius2 = BorderRadius.circular(2);
final sBorderRadius3 = BorderRadius.circular(3);
final sBorderRadius4 = BorderRadius.circular(4);
final sBorderRadius5 = BorderRadius.circular(5);
final sBorderRadius6 = BorderRadius.circular(6);
final sBorderRadius7 = BorderRadius.circular(7);
final sBorderRadius8 = BorderRadius.circular(8);
final sBorderRadius9 = BorderRadius.circular(9);
final sBorderRadius10 = BorderRadius.circular(10);
final sBorderRadius15 = BorderRadius.circular(15);
final sBorderRadius20 = BorderRadius.circular(20);
final sBorderRadius25 = BorderRadius.circular(25);
final sBorderRadius30 = BorderRadius.circular(30);
final sBorderRadius50 = BorderRadius.circular(50);

final sRadius1 = Radius.circular(1);
final sRadius2 = Radius.circular(2);
final sRadius3 = Radius.circular(3);
final sRadius4 = Radius.circular(4);
final sRadius5 = Radius.circular(5);
final sRadius6 = Radius.circular(6);
final sRadius7 = Radius.circular(7);
final sRadius8 = Radius.circular(8);
final sRadius9 = Radius.circular(9);
final sRadius10 = Radius.circular(10);
final sRadius15 = Radius.circular(15);
final sRadius20 = Radius.circular(20);
final sRadius25 = Radius.circular(25);
final sRadius30 = Radius.circular(30);
final sRadius50 = Radius.circular(50);
final sRadiusButton = RoundedRectangleBorder(borderRadius: sBorderRadius7);
final sShadowBox = BoxShadow(
  color: Colors.grey[350],
  blurRadius: 3,
  offset: Offset(1, 1),
);

final sCard = BoxDecoration(color: Colors.white, border: sBorder);

final sForgotPass = TextStyle(fontSize: 12, color: cBlue30);

final sShadow = BoxShadow(
  color: cBlack.withOpacity(.1),
  blurRadius: 3,
  offset: Offset(1, 1),
);
final sShadowElevation = BoxShadow(
  color: cBlue.withOpacity(.1),
  blurRadius: 5,
  spreadRadius: 1,
  offset: Offset(1, 1),
);

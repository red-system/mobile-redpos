import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/home/home_controller.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:redpos/core/services/request_api.dart';
import 'package:redpos/functions/func_checkout.dart';
import 'package:redpos/functions/func_signin.dart';
import 'package:redpos/screens/home/home.dart';
import 'package:redpos/screens/pos/components/pos_print.dart';
import 'package:redpos/screens/print/print.dart';

class CheckoutController extends GetxController with StateMixin {
  RequestApi _insTransaksi = RequestApi();

  @override
  void onInit() {
    change('', status: RxStatus.empty());
    super.onInit();
  }

  void insTransaksi(baseUrl, data) async {
    print(data['transaksi_detail'][0]);
    change('', status: RxStatus.loading());
    final _res = await _insTransaksi.requestApi(url: baseUrl, data: data);

    try {
      if (_res['kode'] == 200) {
        change('', status: RxStatus.success());
        PosController().resetCheckOut(resetListCart: true);
        HomeController().updKas(data['balance']);
        Get.back();
        confirmPrint(data, _res['no_faktur']);

        // Get.to(HomePage());
      } else {
        Get.snackbar(
          'Pesan :',
          _res['message'],
          backgroundColor: Colors.orange,
          colorText: cWhite,
          duration: 2.seconds,
          snackPosition: SnackPosition.BOTTOM,
        );
        change(_res['message'], status: RxStatus.error());
      }
    } catch (e) {
      print(e.toString());
      change(e.toString(), status: RxStatus.error());
      Get.snackbar('Error Message', e.toString());
    }
    update();
  }

  confirmPrint(data, noFaktur) {
    Get.defaultDialog(
      backgroundColor: cWhite,
      title: '',
      titleStyle: TextStyle(fontSize: 0),
      radius: 10,
      content: alertSuccess(data['sisa']),
      confirm: ElevatedButton(
        onPressed: () async {
          // Get.back(result: true);
          Get.to(PrintPage(
            transaksi: data,
            noFaktur: noFaktur,
            bagian: 'checkout',
          ));
          // Get.bottomSheet(
          //   PosPrint(transaksi: data, noFaktur: noFaktur),
          //   isScrollControlled: true,
          // );
        },
        style: ElevatedButton.styleFrom(
          primary: cBlue35,
          shape: RoundedRectangleBorder(borderRadius: sBorderRadius5),
        ),
        child: Text('OK', style: TextStyle(color: cWhite)),
      ),
    ).then((value) {
      // if (value == null)
      // Get.to(PrintPage(data: result, cart: cartResult));

      // Get.bottomSheet(
      //   PosPrint(
      //     result: result,
      //     posController: posController,
      //     cart: cartResult,
      //   ),
      // );
    });
  }
}

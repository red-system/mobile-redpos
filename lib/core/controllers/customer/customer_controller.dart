import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/url.dart';
import 'package:redpos/core/models/customer_response.dart';
import 'package:redpos/core/services/request_api.dart';

class CustomerController extends GetxController with StateMixin {
  RequestApi _getCustomer = RequestApi();

  bool showClearSearch = false;

  @override
  void onInit() {
    change('', status: RxStatus.empty());
    super.onInit();
  }

  void getCustomer() async {
    showClearSearch = false;
    change('', status: RxStatus.loading());
    final _res = await _getCustomer.requestApi(url: uCustomer, data: null);
    try {
      if (_res['kode'] == 200) {
        final _customer = _res['data'] as List;

        customerList =
            _customer.map((data) => CustomerResponse.fromJson(data)).toList();
        customerListSearch = customerList;

        change('', status: RxStatus.success());
      } else {
        Get.snackbar(
          'Pesan :',
          _res['message'],
          backgroundColor: Colors.orange,
          colorText: cWhite,
          duration: 2.seconds,
          snackPosition: SnackPosition.BOTTOM,
        );
        change(_res['message'], status: RxStatus.error());
      }
    } catch (e) {
      print(e.toString());
      change(e.toString(), status: RxStatus.error());
      Get.snackbar('Error Message', e.toString());
    }
    update();
  }

  void searchCustomer(String textSearch) async {
    showClearSearch = textSearch == '' ? false : true;

    customerListSearch = customerList.where((elm) {
      String nama = elm.nama.toString().toLowerCase();

      bool chNama = nama.contains(textSearch.toLowerCase());
      return chNama;
    }).toList();
    update();
  }

  void clearSearchForm(TextEditingController textSearch) async {
    textSearch.text = '';
    update();
  }
}

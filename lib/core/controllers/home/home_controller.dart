import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:intl/intl.dart';
import 'package:redpos/screens/home/home.dart';
import 'package:redpos/screens/print/print.dart';

class HomeController extends GetxController {
  void setKas(String value) async {
    final openDate = DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now());

    final kasNow = double.parse(value);

    final checkKasBefore =
        sess.read('kasBefore') == null ? 0 : sess.read('kasBefore');

    sess.write('openDate', openDate);
    sess.write('jumlahTransaksi', 0);
    sess.write('totalTransaksi', 0);
    sess.write('kasBefore', checkKasBefore);
    sess.write('kasAwal', kasNow);
    sess.write('kasAkhir', kasNow);
    update();
  }

  void updKas(double nilaiTransaksi) async {
    final jumlahTransaksi = sess.read('jumlahTransaksi') + 1;
    final totalTransaksi = sess.read('totalTransaksi') + nilaiTransaksi;
    final kasAkhir = sess.read('kasAkhir') + nilaiTransaksi;

    sess.write('jumlahTransaksi', jumlahTransaksi);
    sess.write('totalTransaksi', totalTransaksi);
    sess.write('kasAkhir', kasAkhir);
    update();
  }

  void updClosing() async {
    final kasAkhir = sess.read('kasAkhir');

    sess.remove('jumlahTransaksi');
    sess.remove('totalTransaksi');
    sess.remove('kasAkhir');
    sess.remove('kasAwal');
    sess.write('kasBefore', kasAkhir);

    // Get.offAll(HomePage());
    // Get.snackbar(
    //   'Pesan :',
    //   'Closing Sukses.',
    //   backgroundColor: Colors.green,
    //   colorText: cWhite,
    //   duration: 2.seconds,
    //   snackPosition: SnackPosition.BOTTOM,
    // );
    // Get.to(PrintPage(transaksi: '', bagian: 'closing'));
    update();
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/url.dart';
import 'package:redpos/core/models/customer_response.dart';
import 'package:redpos/core/models/transaksi_response.dart';
import 'package:redpos/core/services/request_api.dart';

class ListingController extends GetxController with StateMixin {
  RequestApi _getListing = RequestApi();

  @override
  void onInit() {
    change('', status: RxStatus.empty());
    super.onInit();
  }

  void getListing() async {
    change('', status: RxStatus.loading());
    final _res = await _getListing.requestApi(
      url: uListing,
      data: {"id_user": sess.read('idUser')},
    );
    try {
      if (_res['kode'] == 200) {
        final _customer = _res['data']['customer'] as List;
        final _transaksi = _res['data']['transaksi'] as List;

        customerList =
            _customer.map((data) => CustomerResponse.fromJson(data)).toList();
        customerListSearch = customerList;

        transaksiList =
            _transaksi.map((data) => TransaksiResponse.fromJson(data)).toList();

        change('', status: RxStatus.success());
      } else {
        Get.snackbar(
          'Pesan :',
          _res['message'],
          backgroundColor: Colors.orange,
          colorText: cWhite,
          duration: 2.seconds,
          snackPosition: SnackPosition.BOTTOM,
        );
        change(_res['message'], status: RxStatus.error());
      }
    } catch (e) {
      print(e.toString());
      change(e.toString(), status: RxStatus.error());
      Get.snackbar('Error Message', e.toString());
    }
    update();
  }
}

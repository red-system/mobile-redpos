import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:redpos/core/models/customer_response.dart';
import 'package:redpos/core/models/produk_response.dart';
import 'package:redpos/screens/pos/components/cart_produk_item.dart';
import 'package:redpos/screens/pos/components/dialog_trash.dart';
import 'package:redpos/widgets/notify.dart';

class PosController extends GetxController {
  final setSelected = null;
  bool showClearSearch = false;
  double totalCart = 0, biayaTambahan = 0, disc = 0, balance = 0, terbayar = 0;
  String pembayaran = pembayaranList[0];

  @override
  void onInit() {
    // setListProduk();
    getTotalCart();
    super.onInit();
  }

  // void setListProduk() async {
  //   final list = listProduk;

  //   produkList = list.map((data) => ProdukResponse.fromJson(data)).toList();
  //   produkListSearch = produkList;
  //   update();
  // }

  void searchProduk(String textSearch) async {
    showClearSearch = textSearch == '' ? false : true;

    produkListSearch = produkList.where((elm) {
      String nama = elm.nama.toString().toLowerCase();
      String kode = elm.kodeBarang.toString();

      bool chNama = nama.contains(textSearch.toLowerCase());
      bool chKode = kode.contains(textSearch);

      bool res = ((chNama) || (chKode)) ? true : false;
      return res;
    }).toList();
    update();
  }

  void clearSearchForm(TextEditingController textSearch) async {
    textSearch.text = '';
    update();
  }

  void setCustomer(
    TextEditingController namaController,
    TextEditingController alamatController,
    TextEditingController telpController,
    CustomerResponse data,
  ) {
    namaController.text = data.nama;
    alamatController.text = data.alamat;
    telpController.text = data.noTelp;
    update();
  }

  void setKuantiti(String tipe, int index, result) async {
    if (tipe == 'plus') {
      cartList[index].kuantiti = cartList[index].kuantiti + 1;
    }

    if (tipe == 'min') {
      if (cartList[index].kuantiti > 1)
        cartList[index].kuantiti = cartList[index].kuantiti - 1;

      if (cartList[index].kuantiti == 1) {
        Get.bottomSheet(DialogTrash()).then((value) {
          if (value != null) removeCart(result);
        });
      }
    }

    getTotalCart();
    update();
  }

  void removeCart(CartResponse data) async {
    cartList.remove(data);

    getTotalCart();
    update();
  }

  void addToCart(ProdukResponse data) async {
    print(cartList.length);
    final numb = cartList.length;

    int check = 0;
    int loop = 0;
    cartList.forEach((elm) {
      if (data.idBarang == elm.idProduk) {
        cartList[loop].kuantiti = elm.kuantiti + 1;
        check++;
        return 0;
      }
      loop++;
    });

    if (check == 0) {
      final dataNew = {
        "id_cart": numb + 1,
        "id_produk": data.idBarang,
        "kode_produk": data.kodeBarang,
        "nama": data.nama,
        "kuantiti": 1,
        "harga": data.harga,
        "harga_total": data.harga,
      };

      cartList.add(CartResponse.fromJson(dataNew));
    }
    getTotalCart();
    update();
  }

  void getTotalCart() async {
    totalCart = 0;
    cartList.forEach((elm) {
      totalCart += elm.harga * elm.kuantiti;
    });
    update();
  }

  void getTotalCheckOut(String kolom, String value) async {
    value = (value == '') ? '0' : value;

    if (kolom == 'biayatambahan') {
      String nilai = value.replaceAll('.', '');
      nilai = nilai.replaceAll(',', '.');

      biayaTambahan = double.parse(nilai);
    }
    if (kolom == 'disc') {
      String nilai = value.replaceAll('.', '');
      nilai = nilai.replaceAll(',', '.');

      disc = double.parse(nilai);
    }
    if (kolom == 'terbayar') {
      String nilai = value.replaceAll('.', '');
      nilai = nilai.replaceAll(',', '.');

      terbayar = double.parse(nilai);
    }

    if (kolom == 'pembayaran') pembayaran = value;

    balance = totalCart + biayaTambahan - disc;
    update();
  }

  void resetCheckOut({resetListCart = false}) async {
    biayaTambahan = disc = terbayar = 0;
    balance = totalCart;
    pembayaran = pembayaranList[0];

    if (resetListCart) cartList = [];
    update();
  }
}

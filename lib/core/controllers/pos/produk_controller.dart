import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/url.dart';
import 'package:redpos/core/models/produk_response.dart';
import 'package:redpos/core/services/request_api.dart';

class ProdukController extends GetxController with StateMixin {
  RequestApi _getProduk = RequestApi();

  @override
  void onInit() {
    change('', status: RxStatus.empty());
    super.onInit();
  }

  void getProduk() async {
    change('', status: RxStatus.loading());
    final _res = await _getProduk.requestApi(url: uProduk, data: null);
    try {
      if (_res['kode'] == 200) {
        final _produk = _res['data'] as List;

        produkList =
            _produk.map((data) => ProdukResponse.fromJson(data)).toList();
        produkListSearch = produkList;

        change('', status: RxStatus.success());
      } else {
        Get.snackbar(
          'Pesan :',
          _res['message'],
          backgroundColor: Colors.orange,
          colorText: cWhite,
          duration: 2.seconds,
          snackPosition: SnackPosition.BOTTOM,
        );
        change(_res['message'], status: RxStatus.error());
      }
    } catch (e) {
      print(e.toString());
      change(e.toString(), status: RxStatus.error());
      Get.snackbar('Error Message', e.toString());
    }
    update();
  }
}

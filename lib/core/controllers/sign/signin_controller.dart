import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/core/services/request_api.dart';
import 'package:redpos/functions/func_signin.dart';
import 'package:redpos/screens/home/home.dart';

class SignInController extends GetxController with StateMixin {
  RequestApi _signInAccount = RequestApi();

  @override
  void onInit() {
    change('', status: RxStatus.empty());
    super.onInit();
  }

  void toSignIn(baseUrl, data) async {
    change('', status: RxStatus.loading());
    final _res = await _signInAccount.requestApi(url: baseUrl, data: data);
    try {
      if (_res['kode'] == 200) {
        sessionSignIn(_res);
        change('', status: RxStatus.success());
        Get.to(HomePage());
      } else {
        Get.snackbar(
          'Pesan :',
          _res['message'],
          backgroundColor: Colors.orange,
          colorText: cWhite,
          duration: 2.seconds,
          snackPosition: SnackPosition.BOTTOM,
        );
        change(_res['message'], status: RxStatus.error());
      }
    } catch (e) {
      print(e.toString());
      change(e.toString(), status: RxStatus.error());
      Get.snackbar('Error Message', e.toString());
    }
    update();
  }
}

import 'package:get/get.dart';
import 'package:redpos/constants/init.dart';

class TogglePasswordController extends GetxController {
  void setTogglePassword(bool value) {
    hiddenPassword = !value;
    update();
  }

  void resetTogglePassword() {
    hiddenPassword = true;
    update();
  }
}

class CartResponse {
  int idCart;
  int idProduk;
  String kodeProduk;
  String nama;
  double kuantiti;
  double harga;
  double hargaTotal;

  CartResponse(
      {this.idCart,
      this.idProduk,
      this.kodeProduk,
      this.nama,
      this.kuantiti,
      this.harga,
      this.hargaTotal});

  CartResponse.fromJson(Map<String, dynamic> json) {
    idCart = json['id_cart'];
    idProduk = json['id_produk'];
    kodeProduk = json['kode_produk'];
    nama = json['nama'];
    kuantiti = double.parse(json['kuantiti'].toString());
    harga = double.parse(json['harga'].toString());
    hargaTotal = double.parse(json['harga_total'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_cart'] = this.idCart;
    data['id_produk'] = this.idProduk;
    data['kode_produk'] = this.kodeProduk;
    data['nama'] = this.nama;
    data['kuantiti'] = this.kuantiti;
    data['harga'] = this.harga;
    data['harga_total'] = this.hargaTotal;
    return data;
  }
}

class CustomerResponse {
  int idCustomer;
  String nama;
  String noTelp;
  String alamat;
  String lokasi;
  String createdAt;
  String updatedAt;

  CustomerResponse(
      {this.idCustomer,
      this.nama,
      this.noTelp,
      this.alamat,
      this.lokasi,
      this.createdAt,
      this.updatedAt});

  CustomerResponse.fromJson(Map<String, dynamic> json) {
    idCustomer = json['id_customer'];
    nama = json['nama'];
    noTelp = json['no_telp'];
    alamat = json['alamat'];
    lokasi = json['lokasi'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_customer'] = this.idCustomer;
    data['nama'] = this.nama;
    data['no_telp'] = this.noTelp;
    data['alamat'] = this.alamat;
    data['lokasi'] = this.lokasi;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

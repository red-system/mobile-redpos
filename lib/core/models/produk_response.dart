class ProdukResponse {
  int idBarang;
  String kodeBarang;
  String nama;
  String jenisBarang;
  double stok;
  double harga;
  String ketersediaan;
  String createdAt;
  String updatedAt;

  ProdukResponse(
      {this.idBarang,
      this.kodeBarang,
      this.nama,
      this.jenisBarang,
      this.stok,
      this.harga,
      this.ketersediaan,
      this.createdAt,
      this.updatedAt});

  ProdukResponse.fromJson(Map<String, dynamic> json) {
    idBarang = json['id_barang'];
    kodeBarang = json['kode_barang'];
    nama = json['nama'];
    jenisBarang = json['jenis_barang'];
    stok = double.parse(json['stok'].toString());
    harga = double.parse(json['harga'].toString());
    ketersediaan = json['ketersediaan'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_barang'] = this.idBarang;
    data['kode_barang'] = this.kodeBarang;
    data['nama'] = this.nama;
    data['jenis_barang'] = this.jenisBarang;
    data['stok'] = this.stok;
    data['harga'] = this.harga;
    data['ketersediaan'] = this.ketersediaan;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

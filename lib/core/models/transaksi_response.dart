class TransaksiResponse {
  int idTransaksi;
  String noFaktur;
  String namaCst;
  String noTelpCst;
  String alamatCst;
  String lokasiCst;
  int idUser;
  double kasAwal;
  double kasAkhir;
  double totalTransaksi;
  double biayaTambahan;
  double disc;
  double balance;
  double bayar;
  String jenisPembayaran;
  String status;
  String dateTransaksi;
  String createdAt;
  String updatedAt;
  List<TransaksiDetail> transaksiDetail;

  TransaksiResponse(
      {this.idTransaksi,
      this.noFaktur,
      this.namaCst,
      this.noTelpCst,
      this.alamatCst,
      this.lokasiCst,
      this.idUser,
      this.kasAwal,
      this.kasAkhir,
      this.totalTransaksi,
      this.biayaTambahan,
      this.disc,
      this.balance,
      this.bayar,
      this.jenisPembayaran,
      this.status,
      this.dateTransaksi,
      this.createdAt,
      this.updatedAt,
      this.transaksiDetail});

  TransaksiResponse.fromJson(Map<String, dynamic> json) {
    idTransaksi = json['id_transaksi'];
    noFaktur = json['no_faktur'];
    namaCst = json['nama_cst'];
    noTelpCst = json['no_telp_cst'];
    alamatCst = json['alamat_cst'];
    lokasiCst = json['lokasi_cst'];
    idUser = json['id_user'];
    kasAwal = double.parse(json['kas_awal'].toString());
    kasAkhir = double.parse(json['kas_akhir'].toString());
    totalTransaksi = double.parse(json['total_transaksi'].toString());
    biayaTambahan = double.parse(json['biaya_tambahan'].toString());
    disc = double.parse(json['disc'].toString());
    balance = double.parse(json['balance'].toString());
    bayar = double.parse(json['bayar'].toString());
    jenisPembayaran = json['jenis_pembayaran'];
    status = json['status'];
    dateTransaksi = json['date_transaksi'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['transaksi_detail'] != null) {
      transaksiDetail = [];
      json['transaksi_detail'].forEach((v) {
        transaksiDetail.add(new TransaksiDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_transaksi'] = this.idTransaksi;
    data['no_faktur'] = this.noFaktur;
    data['nama_cst'] = this.namaCst;
    data['no_telp_cst'] = this.noTelpCst;
    data['alamat_cst'] = this.alamatCst;
    data['lokasi_cst'] = this.lokasiCst;
    data['id_user'] = this.idUser;
    data['kas_awal'] = this.kasAwal;
    data['kas_akhir'] = this.kasAkhir;
    data['total_transaksi'] = this.totalTransaksi;
    data['biaya_tambahan'] = this.biayaTambahan;
    data['disc'] = this.disc;
    data['balance'] = this.balance;
    data['bayar'] = this.bayar;
    data['jenis_pembayaran'] = this.jenisPembayaran;
    data['status'] = this.status;
    data['date_transaksi'] = this.dateTransaksi;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.transaksiDetail != null) {
      data['transaksi_detail'] =
          this.transaksiDetail.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TransaksiDetail {
  int idTransaksiDetail;
  int idTransaksi;
  String noFaktur;
  int idBarang;
  double qty;
  double harga;
  double hargaTotal;
  String dateTransaksi;
  String createdAt;
  String updatedAt;

  TransaksiDetail(
      {this.idTransaksiDetail,
      this.idTransaksi,
      this.noFaktur,
      this.idBarang,
      this.qty,
      this.harga,
      this.hargaTotal,
      this.dateTransaksi,
      this.createdAt,
      this.updatedAt});

  TransaksiDetail.fromJson(Map<String, dynamic> json) {
    idTransaksiDetail = json['id_transaksi_detail'];
    idTransaksi = json['id_transaksi'];
    noFaktur = json['no_faktur'];
    idBarang = json['id_barang'];
    qty = double.parse(json['qty'].toString());
    harga = double.parse(json['harga'].toString());
    hargaTotal = double.parse(json['harga_total'].toString());
    dateTransaksi = json['date_transaksi'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_transaksi_detail'] = this.idTransaksiDetail;
    data['id_transaksi'] = this.idTransaksi;
    data['no_faktur'] = this.noFaktur;
    data['id_barang'] = this.idBarang;
    data['qty'] = this.qty;
    data['harga'] = this.harga;
    data['harga_total'] = this.hargaTotal;
    data['date_transaksi'] = this.dateTransaksi;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

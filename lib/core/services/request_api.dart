import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class RequestApi {
  Dio _dio = new Dio();

  Future<dynamic> requestApi(
      {@required dynamic data, @required String url}) async {
    Response _response;
    FormData _resData;

    _resData = (data == null) ? _resData : new FormData.fromMap(data);

    try {
      _response = await _dio.post(url, data: _resData);

      return _response.data;
    } on DioError catch (e) {
      print(e.response.toString());
      String errorMessage = 'System error';
      switch (e.type) {
        case DioErrorType.CONNECT_TIMEOUT:
          errorMessage = 'Connection timeout.';
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorMessage = 'Send request timeout.';
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorMessage = 'Receive request timeout.';
          break;
        case DioErrorType.RESPONSE:
          errorMessage = 'Not response.';
          break;
        case DioErrorType.CANCEL:
          errorMessage = 'Cancel request.';
          break;
        case DioErrorType.DEFAULT:
          errorMessage = 'System not connection.';
          break;
      }

      var err = {'kode': 400, 'message': errorMessage};
      return err;
    }
  }
}

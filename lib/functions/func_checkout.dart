import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/controllers/home/home_controller.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:redpos/screens/pos/components/pos_print.dart';
import 'package:redpos/screens/print/print.dart';
import 'package:redpos/widgets/notify.dart';

prosesCheckout(
  PosController data,
  PosController posController,
  dataCustomer,
) async {
  double balance = data.totalCart + data.biayaTambahan - data.disc;
  double sisa = data.terbayar - balance;

  if (dataCustomer['nm_customer'] == '') {
    Notify(
      message: 'Nama customer tidak boleh kosong.',
      color: Colors.orange,
    );
    return 0;
  } else if (sisa < 0) {
    Notify(
      message: 'Uang terbayar kurang.',
      color: Colors.orange,
    );
    return 0;
  } else {
    var result = {
      'customer': dataCustomer,
      'id_user': sess.read('idUser'),
      'total': data.totalCart,
      'biaya_tambahan': data.biayaTambahan,
      'disc': data.disc,
      'balance': balance,
      'terbayar': data.terbayar,
      'sisa': sisa,
      'pembayaran': data.pembayaran,
      'kasAwal': sess.read('kasAwal'),
      'kasAkhir': sess.read('kasAkhir'),
    };

    // Get.back();
    // Get.defaultDialog(
    //   backgroundColor: cWhite,
    //   title: '',
    //   titleStyle: TextStyle(fontSize: 0),
    //   radius: 10,
    //   content: alertSuccess(sisa),
    //   confirm: ElevatedButton(
    //     onPressed: () async {
    //       Get.back(result: true);
    //       // Get.to(PrintPage(data: result, cart: cartResult));
    //       Get.bottomSheet(
    //         PosPrint(
    //           result: result,
    //           posController: posController,
    //           cart: cartResult,
    //         ),
    //         isScrollControlled: true,
    //       );
    //     },
    //     style: ElevatedButton.styleFrom(
    //       primary: cBlue35,
    //       shape: RoundedRectangleBorder(borderRadius: sBorderRadius5),
    //     ),
    //     child: Text('OK', style: TextStyle(color: cWhite)),
    //   ),
    // ).then((value) {
    //   // if (value == null)
    //   // Get.to(PrintPage(data: result, cart: cartResult));

    //   // Get.bottomSheet(
    //   //   PosPrint(
    //   //     result: result,
    //   //     posController: posController,
    //   //     cart: cartResult,
    //   //   ),
    //   // );
    // });
    return result;
  }
}

Column alertSuccess(double sisa) {
  return Column(
    children: [
      CircleAvatar(
        backgroundColor: Colors.green,
        radius: 30,
        child: Icon(Icons.check, color: cWhite, size: 28),
      ),
      SizedBox(height: 20),
      Text(
        'Rp ${money.format(sisa)}',
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: cBlack80,
        ),
      ),
      SizedBox(height: 10),
      Text(
        'sisa kembalian',
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w300,
          color: cBlack50,
        ),
      ),
    ],
  );
}

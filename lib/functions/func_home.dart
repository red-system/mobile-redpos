import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/home/home_controller.dart';
import 'package:redpos/screens/home/components/form_kas.dart';
import 'package:redpos/screens/pos/pos.dart';
import 'package:redpos/screens/signin/signin.dart';
import 'package:redpos/widgets/notify.dart';

homeToPos(GlobalKey<FormBuilderState> formBuilderKey,
    HomeController _homeController) async {
  if (sess.read('kasAwal') == null) {
    Get.bottomSheet(
      FormKas(
        press: () async {
          if (formBuilderKey.currentState.saveAndValidate()) {
            var data = formBuilderKey.currentState.value;
            var kasNow = data['nm_kasnow'];

            kasNow = kasNow.replaceAll('.', '');
            kasNow = kasNow.replaceAll(',', '.');

            if (sess.read('kasBefore') > double.parse(kasNow)) {
              Notify(
                message:
                    'Minimal kas saat ini Rp. ${money.format(sess.read('kasBefore'))}',
                color: Colors.orange,
              );
            } else {
              _homeController.setKas(kasNow);
              Get.back();
              Get.to(PosPage());
              Notify(
                message: 'Kas saat ini berhasil disimpan',
                color: Colors.green,
              );
            }
          }
        },
        formBuilderKey: formBuilderKey,
      ),
    );
  } else {
    Get.to(PosPage());
  }
}

homeToSignOut() {
  print(sess.read('kasAwal'));
  if (sess.read('kasAwal') == null) {
    Get.defaultDialog(
      backgroundColor: cWhite,
      title: '',
      titleStyle: TextStyle(fontSize: 0),
      radius: 10,
      content: Padding(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 10),
        child: Column(
          children: [
            Text(
              'Apakah anda yakin ingin keluar ?',
              style: TextStyle(fontSize: 14),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            Image.asset('assets/images/signout.png')
          ],
        ),
      ),
      confirm: ElevatedButton(
        onPressed: () {
          sess.erase();
          Get.offAll(SignInPage());
        },
        style: ElevatedButton.styleFrom(
          primary: cBlue35,
          shape: RoundedRectangleBorder(borderRadius: sBorderRadius5),
        ),
        child: Text('Iya', style: TextStyle(color: cWhite)),
      ),
      cancel: ElevatedButton(
        onPressed: () => Get.back(),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius: sBorderRadius5),
          primary: cGrey10,
        ),
        child: Text('Tidak', style: TextStyle(color: cBlack80)),
      ),
    );
  } else {
    Get.defaultDialog(
      backgroundColor: cWhite,
      title: '',
      titleStyle: TextStyle(fontSize: 0),
      radius: 10,
      content: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          children: [
            Icon(Icons.highlight_off, color: cRed35, size: 70),
            SizedBox(height: 20),
            Text(
              'Logout Error',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 10),
            Text(
              'Lakukan closing di POS terlebih dahulu',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w300,
                color: cBlack80.withOpacity(.7),
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 15),
            ElevatedButton(onPressed: () => Get.back(), child: Text('OK'))
          ],
        ),
      ),
    );
  }
}

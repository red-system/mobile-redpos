import 'package:redpos/constants/init.dart';

checkFormSignIn(formBuilderKey) =>
    (formBuilderKey.currentState.saveAndValidate())
        ? formBuilderKey.currentState.value
        : null;

sessionSignIn(user) {
  sess.write('isLogin', true);
  sess.write('idUser', user['data']['id_user']);
  sess.write('namaUser', user['data']['nama']);
  sess.write('username', user['data']['username']);
  sess.write('password', user['data']['password']);
  sess.write('jabatan', user['data']['jabatan']);
  sess.write('noTelp', user['data']['no_telp']);
  sess.write('alamat', user['data']['alamat']);
  sess.write('kasBefore', user['kasAkhir']);
}

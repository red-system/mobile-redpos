import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:redpos/screens/app_widget.dart';

void main() async {
  await GetStorage.init();
  await initializeDateFormatting('id_ID', null)
      .then((_) => runApp(AppWidget()));
}

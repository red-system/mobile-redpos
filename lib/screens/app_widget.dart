import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/screens/home/home.dart';
import 'package:redpos/screens/signin/signin.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final statusLogin =
        sess.read('isLogin') == null ? false : sess.read('isLogin');
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Red Pos',
      theme: ThemeData(
        primarySwatch: MaterialColor(0xFF4d79ff, cPrimarySwatch), // cRed35
        primaryColorBrightness: Brightness.dark,
        appBarTheme: AppBarTheme(brightness: Brightness.dark),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: statusLogin ? HomePage() : SignInPage(),
    );
  }
}

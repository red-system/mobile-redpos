import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/functions/currency.dart';

class FormKas extends StatelessWidget {
  const FormKas({
    Key key,
    @required this.press,
    this.formBuilderKey,
  }) : super(key: key);

  final Function press;
  final GlobalKey<FormBuilderState> formBuilderKey;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => focusScope(context),
      child: Container(
        decoration: BoxDecoration(
          color: cWhite,
          borderRadius: BorderRadius.only(
            topLeft: sRadius20,
            topRight: sRadius20,
          ),
        ),
        padding: sPaddingHor15,
        child: FormBuilder(
          key: formBuilderKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 20),
                Text(
                  'Input Kas Saat Ini',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: cBlack80,
                  ),
                ),
                SizedBox(height: 30),
                Image.asset(
                  'assets/images/input.png',
                  width: Get.width * .6,
                ),
                SizedBox(height: 30),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  decoration: BoxDecoration(
                    color: cBlack05,
                    borderRadius: sBorderRadius5,
                  ),
                  child: Row(
                    children: [
                      Text(
                        'Kas Sebelumnya : ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('Rp. ${money.format(sess.read('kasBefore'))}'),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                FormBuilderTextField(
                  name: 'nm_kasnow',
                  style: TextStyle(fontSize: 14),
                  validator: FormBuilderValidators.required(context),
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    CurrencyFormat(),
                  ],
                  decoration: InputDecoration(
                    isDense: true,
                    contentPadding: sPaddingAll15,
                    hintText: 'Min: ${money.format(sess.read('kasBefore'))}',
                    hintStyle: TextStyle(fontSize: 14),
                    border: OutlineInputBorder(),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: cBlack10),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                ElevatedButton(
                  onPressed: press,
                  child: SizedBox(
                    width: double.infinity,
                    child: Text('Simpan', textAlign: TextAlign.center),
                  ),
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/home/home_controller.dart';
import 'package:redpos/functions/func_home.dart';
import 'package:intl/intl.dart';
import 'package:redpos/screens/listing/listing.dart';

class HomeMenuItem extends StatefulWidget {
  const HomeMenuItem({
    Key key,
    @required this.index,
    @required this.data,
  }) : super(key: key);

  final int index;
  final data;

  @override
  _HomeMenuItemState createState() => _HomeMenuItemState();
}

class _HomeMenuItemState extends State<HomeMenuItem> {
  final GlobalKey<FormBuilderState> formBuilderKey =
      GlobalKey<FormBuilderState>();

  HomeController _homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: sPaddingAll10,
      child: CupertinoButton(
        onPressed: () {
          if (widget.index == 0) homeToPos(formBuilderKey, _homeController);
          if (widget.index == 1) Get.to(ListingPage());
          if (widget.index == 2) homeToSignOut();
        },
        padding: sNoPadding,
        child: Container(
          width: double.infinity,
          padding: sPaddingAll10,
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: sBorderRadius10,
            boxShadow: [sShadowElevation],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: widget.index == 2 ? sPaddingAll5 : sNoPadding,
                  child: Image.asset(
                    'assets/images/${widget.data['img']}',
                  ),
                ),
              ),
              SizedBox(height: 10),
              Text(
                widget.data['nama'],
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: cBlack80,
                  letterSpacing: 0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

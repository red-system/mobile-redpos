import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/screens/home/components/home_menu_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: Scaffold(
        body: Column(
          children: [
            AppBar(
              automaticallyImplyLeading: false,
              title: Text(
                'Red Pos',
                style: TextStyle(color: cWhite),
              ),
              centerTitle: true,
            ),
            Expanded(
              child: GridView.builder(
                itemCount: listHome.length,
                physics: ClampingScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                padding: sPaddingAll10,
                itemBuilder: (BuildContext context, int index) => HomeMenuItem(
                  index: index,
                  data: listHome[index],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

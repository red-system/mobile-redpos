import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/customer/customer_controller.dart';
import 'package:redpos/core/models/customer_response.dart';
import 'package:redpos/widgets/loading_proses_custom.dart';

class CustomerPage extends StatefulWidget {
  CustomerPage({Key key, this.tipe = 'show'}) : super(key: key);
  final String tipe;

  @override
  _CustomerPageState createState() => _CustomerPageState();
}

class _CustomerPageState extends State<CustomerPage> {
  CustomerController _customerController = Get.put(CustomerController());

  @override
  void initState() {
    _customerController.getCustomer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Customer'),
      ),
      body: _customerController.obx(
        (state) => CustomerList(
            customerController: _customerController, tipe: widget.tipe),
        onLoading: CustomLoadingProses(),
        onEmpty: SizedBox(),
        onError: (err) {
          print(err.toString());
          return SizedBox();
        },
      ),
    );
  }
}

class CustomerList extends StatefulWidget {
  const CustomerList({
    Key key,
    @required this.customerController,
    @required this.tipe,
  }) : super(key: key);

  final CustomerController customerController;
  final String tipe;

  @override
  _CustomerListState createState() => _CustomerListState();
}

class _CustomerListState extends State<CustomerList> {
  TextEditingController _searchCustomerController = TextEditingController();

  @override
  void dispose() {
    _searchCustomerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => focusScope(context),
      child: GetBuilder<CustomerController>(
        builder: (_) {
          return Column(
            children: [
              Container(
                color: cWhite,
                padding: sPaddingAll15,
                child: FormBuilderTextField(
                  name: 'nm_searchproduk',
                  controller: _searchCustomerController,
                  style: TextStyle(fontSize: 12),
                  onChanged: (text) =>
                      widget.customerController.searchCustomer(text),
                  decoration: InputDecoration(
                    isDense: true,
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 15,
                    ),
                    hintText: 'Search Customer',
                    hintStyle: TextStyle(fontSize: 12),
                    prefixIcon: Padding(
                      padding: sPaddingHor15,
                      child: Icon(Icons.search),
                    ),
                    suffixIcon: _.showClearSearch
                        ? Padding(
                            padding: sPaddingHor15,
                            child: CupertinoButton(
                              onPressed: () => widget.customerController
                                  .clearSearchForm(_searchCustomerController),
                              padding: sNoPadding,
                              minSize: 5,
                              child: Icon(Icons.close,
                                  size: 16, color: Colors.redAccent),
                            ),
                          )
                        : SizedBox(),
                    prefixIconConstraints: BoxConstraints(minWidth: 0),
                    suffixIconConstraints: BoxConstraints(minWidth: 0),
                    border: OutlineInputBorder(),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: sBorderRadius5,
                      borderSide: BorderSide(
                        color: cBlack20,
                      ),
                    ),
                  ),
                ),
              ),
              Divider(height: 1),
              Expanded(
                child: Container(
                  color: cWhite,
                  child: ListView.builder(
                    // padding: sPaddingAll15,
                    itemCount: customerListSearch.length,
                    itemBuilder: (_, index) => CustomerListItem(
                      press: () {
                        if (widget.tipe == 'select')
                          Get.back(result: customerListSearch[index]);
                      },
                      index: index,
                      data: customerListSearch[index],
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class CustomerListItem extends StatelessWidget {
  const CustomerListItem({
    Key key,
    @required this.index,
    @required this.data,
    @required this.press,
  }) : super(key: key);

  final int index;
  final CustomerResponse data;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        index == 0 ? SizedBox() : Divider(height: 1),
        TextButton(
          onPressed: press,
          style: TextButton.styleFrom(padding: sNoPadding),
          child: Container(
            padding: sPaddingAll15,
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.nama,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: cBlack80,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        data.noTelp,
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          color: cBlack80,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10),
                Icon(Icons.arrow_forward, size: 20),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

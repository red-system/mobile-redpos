import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/customer/customer_controller.dart';
import 'package:redpos/core/controllers/listing/listing_controller.dart';
import 'package:redpos/screens/listing/customer/customer.dart';
import 'package:redpos/screens/listing/transaksi/transaksi.dart';
import 'package:redpos/widgets/loading_proses_custom.dart';

class ListingPage extends StatefulWidget {
  const ListingPage({Key key}) : super(key: key);

  @override
  _ListingPageState createState() => _ListingPageState();
}

class _ListingPageState extends State<ListingPage> {
  ListingController _listingController = Get.put(ListingController());

  @override
  void initState() {
    _listingController.getListing();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listing'),
      ),
      body: _listingController.obx(
        (state) => ListingList(),
        onLoading: CustomLoadingProses(),
        onEmpty: SizedBox(),
        onError: (err) {
          print(err.toString());
          return SizedBox();
        },
      ),
    );
  }
}

class ListingList extends StatelessWidget {
  const ListingList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: sPaddingHor10,
      child: Column(
        children: [
          SizedBox(height: 5),
          ListingListItem(
            press: () => Get.to(CustomerPage()),
            title: 'Listing Customer',
            subTitle: '${customerList.length} data',
          ),
          ListingListItem(
            press: () => Get.to(TransaksiPage()),
            title: 'Listing Transaksi',
            subTitle: '${transaksiList.length} data',
            color: Colors.green,
          ),
        ],
      ),
    );
  }
}

class ListingListItem extends StatelessWidget {
  const ListingListItem({
    Key key,
    @required this.press,
    @required this.title,
    @required this.subTitle,
    this.color = cBlue35,
  }) : super(key: key);

  final Function press;
  final String title, subTitle;
  final color;

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: press,
      padding: sNoPadding,
      child: Container(
        margin: sPaddingVer5,
        decoration: BoxDecoration(
          color: cWhite,
          borderRadius: sBorderRadius5,
          boxShadow: [sShadow],
        ),
        child: IntrinsicHeight(
          child: Row(
            children: [
              Container(
                width: 5,
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.only(
                    topLeft: sRadius5,
                    bottomLeft: sRadius5,
                  ),
                ),
              ),
              Expanded(
                child: ListTile(
                  title: Text(
                    title,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: cBlack80,
                      letterSpacing: 0,
                    ),
                  ),
                  subtitle: Text(
                    subTitle,
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                      color: cBlack80,
                      letterSpacing: 0,
                    ),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: cBlack60,
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/models/transaksi_response.dart';

class TransaksiListItem extends StatelessWidget {
  const TransaksiListItem({
    Key key,
    @required this.index,
    @required this.data,
  }) : super(key: key);

  final int index;
  final TransaksiResponse data;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        index == 0
            ? SizedBox()
            : Container(width: double.infinity, height: 3, color: cBlack05),
        TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(padding: sNoPadding),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.namaCst,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: cBlack80,
                        ),
                      ),
                      SizedBox(height: 3),
                      Text(
                        data.noFaktur,
                        style: TextStyle(fontSize: 12, color: cBlack80),
                      ),
                      SizedBox(height: 10),
                      Text(
                        datetime.format(DateTime.parse(data.dateTransaksi)),
                        style: TextStyle(
                          fontSize: 10,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w300,
                          color: cBlack80,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10),
                Icon(Icons.arrow_forward, size: 20),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

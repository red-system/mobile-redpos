import 'package:flutter/material.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/screens/listing/transaksi/components/transaksi_list_item.dart';

class TransaksiPage extends StatelessWidget {
  const TransaksiPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Transaksi'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: transaksiList.length,
          itemBuilder: (_, index) => TransaksiListItem(
            index: index,
            data: transaksiList[index],
          ),
        ),
      ),
    );
  }
}

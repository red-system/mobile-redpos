import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/core/controllers/checkout/checkout_controller.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/pos/checkout/components/pos_confirm_checkout.dart';
import 'package:redpos/screens/pos/checkout/components/pos_data_customer.dart';
import 'package:redpos/screens/pos/checkout/components/pos_data_pembayaran.dart';
import 'package:redpos/widgets/loading_proses_custom.dart';

class CheckoutPage extends StatefulWidget {
  const CheckoutPage({
    Key key,
    this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  CheckoutController _checkoutController = Get.put(CheckoutController());

  final GlobalKey<FormBuilderState> formCustomerBuilderKey =
      GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => focusScope(context),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Checkout'),
        ),
        body: Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PosDataCustomer(
                      formCustomerBuilderKey: formCustomerBuilderKey,
                    ),
                    Divider(height: 1),
                    SizedBox(height: 15),
                    PosDataPembayaran(widget: widget),
                    SizedBox(height: 15),
                    PosConfirmCheckout(
                      formCustomerBuilderKey: formCustomerBuilderKey,
                      widget: widget,
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
            _checkoutController.obx(
              (state) => SizedBox(),
              onLoading: CustomLoadingProses(),
              onEmpty: SizedBox(),
              onError: (err) {
                print(err.toString());
                return SizedBox();
              },
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';

class CheckoutConfirm extends StatelessWidget {
  const CheckoutConfirm({
    Key key,
    this.btnText = 'Batal',
    this.colorButton = cBlack10,
    this.colorText = cBlack80,
    @required this.press,
  }) : super(key: key);

  final String btnText;
  final colorButton, colorText;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: press,
      style: ElevatedButton.styleFrom(
        primary: colorButton,
        shape: RoundedRectangleBorder(
          borderRadius: sBorderRadius5,
        ),
      ),
      child: Text(
        btnText,
        style: TextStyle(color: colorText, fontSize: 12),
      ),
    );
  }
}

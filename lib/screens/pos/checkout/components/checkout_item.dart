import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/functions/currency.dart';

class CheckoutItem extends StatelessWidget {
  const CheckoutItem({
    Key key,
    @required this.title,
    this.value = 0,
    this.tipe = 'text',
    @required this.posController,
  }) : super(key: key);

  final String title, tipe;
  final double value;
  final PosController posController;

  @override
  Widget build(BuildContext context) {
    final _name = title.toLowerCase().replaceAll(' ', '');
    return Padding(
      padding: sPaddingVer5,
      child: Row(
        children: [
          SizedBox(
            width: Get.width * .3,
            child: Text(
              title,
              style: TextStyle(fontSize: 12),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: tipe == 'form'
                ? FormBuilderTextField(
                    name: 'nm_$_name',
                    style: TextStyle(fontSize: 14),
                    initialValue: money.format(value),
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      CurrencyFormat()
                    ],
                    onChanged: (value) =>
                        posController.getTotalCheckOut(_name, value),
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: '0',
                      hintStyle: TextStyle(fontSize: 14),
                      contentPadding: sPaddingAll10,
                      border: OutlineInputBorder(),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: sBorderRadius5,
                        borderSide: BorderSide(color: cBlack15),
                      ),
                    ),
                    keyboardType: TextInputType.number,
                  )
                : Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    decoration: BoxDecoration(
                      color: cBlack05,
                      borderRadius: sBorderRadius5,
                    ),
                    child: Text(
                      money.format(value),
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
          )
        ],
      ),
    );
  }
}

class CartCheckoutSelect extends StatelessWidget {
  const CartCheckoutSelect({
    Key key,
    @required this.title,
    @required this.posController,
    @required this.items,
  }) : super(key: key);

  final String title;
  final PosController posController;
  final List items;

  @override
  Widget build(BuildContext context) {
    final _name = title.toLowerCase().replaceAll(' ', '');
    return Padding(
      padding: sPaddingVer5,
      child: Row(
        children: [
          SizedBox(
            width: Get.width * .3,
            child: Text(
              title,
              style: TextStyle(fontSize: 12),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: FormBuilderDropdown(
              name: 'nm_$_name',
              style: TextStyle(fontSize: 14),
              initialValue: items[0],
              onChanged: (value) =>
                  posController.getTotalCheckOut(_name, value),
              items: items
                  .map((value) => DropdownMenuItem(
                        value: value,
                        child:
                            Text('$value', style: TextStyle(color: cBlack80)),
                      ))
                  .toList(),
              decoration: InputDecoration(
                isDense: true,
                hintText: 'Pilih Pembayaran',
                hintStyle: TextStyle(fontSize: 14),
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(
                  borderRadius: sBorderRadius5,
                  borderSide: BorderSide(color: cBlack15),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/constants/url.dart';
import 'package:redpos/core/controllers/checkout/checkout_controller.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:redpos/functions/func_checkout.dart';
import 'package:redpos/screens/pos/checkout/checkout.dart';
import 'package:redpos/screens/pos/checkout/components/checkout_confirm.dart';

class PosConfirmCheckout extends StatefulWidget {
  const PosConfirmCheckout({
    Key key,
    @required this.formCustomerBuilderKey,
    @required this.widget,
  }) : super(key: key);

  final GlobalKey<FormBuilderState> formCustomerBuilderKey;
  final CheckoutPage widget;

  @override
  _PosConfirmCheckoutState createState() => _PosConfirmCheckoutState();
}

class _PosConfirmCheckoutState extends State<PosConfirmCheckout> {
  CheckoutController _checkoutController = Get.put(CheckoutController());
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: sNoPadding,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          CheckoutConfirm(press: () => Get.back()),
          SizedBox(width: 10),
          GetBuilder<PosController>(
            builder: (_) {
              return CheckoutConfirm(
                press: () {
                  widget.formCustomerBuilderKey.currentState.save();
                  var dt = widget.formCustomerBuilderKey.currentState.value;
                  prosesCheckout(_, widget.widget.posController, dt).then(
                    (value) {
                      if (value != 0) {
                        var list = [];
                        list.addAll(cartList.map((e) => e.toJson()));
                        value['transaksi_detail'] = list;
                        _checkoutController.insTransaksi(uInsTransaksi, value);
                      }
                    },
                  );
                },
                btnText: 'Proses',
                colorButton: cBlue35,
                colorText: cWhite,
              );
            },
          ),
        ],
      ),
    );
  }
}

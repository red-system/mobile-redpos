import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/screens/pos/checkout/components/pos_data_form_customer.dart';
import 'package:redpos/widgets/line_title.dart';

class PosDataCustomer extends StatelessWidget {
  const PosDataCustomer({
    Key key,
    @required this.formCustomerBuilderKey,
  }) : super(key: key);

  final GlobalKey<FormBuilderState> formCustomerBuilderKey;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: sNoPadding,
      child: Container(
        child: ExpandableNotifier(
          initialExpanded: true,
          child: ScrollOnExpand(
            scrollOnExpand: true,
            scrollOnCollapse: false,
            child: ExpandablePanel(
              collapsed: SizedBox(),
              theme: const ExpandableThemeData(
                headerAlignment: ExpandablePanelHeaderAlignment.center,
                tapBodyToCollapse: false,
              ),
              header: Padding(
                padding: sPaddingVer10,
                child: Text(
                  'Data Customer',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              expanded: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  LineTitle(width: Get.width * .1),
                  SizedBox(height: 10),
                  PosFormCustomer(
                    formCustomerBuilderKey: formCustomerBuilderKey,
                  ),
                  SizedBox(height: 15),
                ],
              ),
              builder: (_, collapsed, expanded) {
                return IntrinsicHeight(
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                    theme: ExpandableThemeData(crossFadePoint: 0),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

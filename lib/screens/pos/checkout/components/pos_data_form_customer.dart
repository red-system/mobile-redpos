import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/listing/customer/customer.dart';

class PosFormCustomer extends StatefulWidget {
  const PosFormCustomer({
    Key key,
    @required this.formCustomerBuilderKey,
  }) : super(key: key);

  final GlobalKey<FormBuilderState> formCustomerBuilderKey;

  @override
  _PosFormCustomerState createState() => _PosFormCustomerState();
}

class _PosFormCustomerState extends State<PosFormCustomer> {
  TextEditingController _namaController = TextEditingController();
  TextEditingController _alamatController = TextEditingController();
  TextEditingController _teleponController = TextEditingController();

  @override
  void initState() {
    _namaController.text = 'Guest';
    super.initState();
  }

  @override
  void dispose() {
    _namaController.dispose();
    _alamatController.dispose();
    _teleponController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: sNoPadding,
      child: GetBuilder<PosController>(
        builder: (_) {
          return FormBuilder(
            key: widget.formCustomerBuilderKey,
            child: Column(
              children: [
                SizedBox(height: 5),
                CustomerFormNama(
                  namaController: _namaController,
                  alamatController: _alamatController,
                  teleponController: _teleponController,
                  require: true,
                ),
                SizedBox(height: 15),
                CustomerFormAlamat(alamatController: _alamatController),
                SizedBox(height: 10),
                CustomerFormLokasi(),
                SizedBox(height: 10),
                CustomerFormTelepon(teleponController: _teleponController),
              ],
            ),
          );
        },
      ),
    );
  }
}

class CustomerFormTelepon extends StatelessWidget {
  const CustomerFormTelepon({
    Key key,
    @required this.teleponController,
  }) : super(key: key);

  final TextEditingController teleponController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Telepon',
          style: TextStyle(fontWeight: FontWeight.w300),
        ),
        SizedBox(height: 5),
        FormBuilderTextField(
          name: 'nm_telepon',
          controller: teleponController,
          style: TextStyle(fontSize: 14),
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 13),
            hintText: 'Cth: 081092******',
            hintStyle: TextStyle(fontSize: 14),
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(
              borderRadius: sBorderRadius5,
              borderSide: BorderSide(color: cBlack10),
            ),
          ),
          keyboardType: TextInputType.phone,
        ),
      ],
    );
  }
}

class CustomerFormLokasi extends StatelessWidget {
  const CustomerFormLokasi({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Lokasi',
          style: TextStyle(fontWeight: FontWeight.w300),
        ),
        SizedBox(height: 5),
        FormBuilderDropdown(
          name: 'nm_lokasi',
          style: TextStyle(fontSize: 14),
          allowClear: true,
          clearIcon: Icon(Icons.close, size: 18, color: cRed35),
          initialValue: 'Outlet Utama',
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 13),
            // hintText: 'Outlet Utama',
            // hintStyle: TextStyle(fontSize: 14),
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(
              borderRadius: sBorderRadius5,
              borderSide: BorderSide(color: cBlack10),
            ),
          ),
          items: [
            DropdownMenuItem(
              value: 'Outlet Utama',
              child: Text(
                'Outlet Utama',
                style: TextStyle(color: cBlack80),
              ),
            )
          ],
        ),
      ],
    );
  }
}

class CustomerFormAlamat extends StatelessWidget {
  const CustomerFormAlamat({
    Key key,
    @required this.alamatController,
  }) : super(key: key);

  final TextEditingController alamatController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Alamat',
          style: TextStyle(fontWeight: FontWeight.w300),
        ),
        SizedBox(height: 5),
        FormBuilderTextField(
          name: 'nm_alamat',
          controller: alamatController,
          style: TextStyle(fontSize: 14),
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 13),
            hintText: 'Cth: Jalan Renon',
            hintStyle: TextStyle(fontSize: 14),
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(
              borderRadius: sBorderRadius5,
              borderSide: BorderSide(color: cBlack10),
            ),
          ),
        ),
      ],
    );
  }
}

class CustomerFormNama extends StatelessWidget {
  const CustomerFormNama({
    Key key,
    @required this.namaController,
    @required this.alamatController,
    @required this.teleponController,
    this.require = false,
  }) : super(key: key);

  final TextEditingController namaController;
  final TextEditingController alamatController;
  final TextEditingController teleponController;
  final bool require;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Customer',
          style: TextStyle(fontWeight: FontWeight.w300),
        ),
        SizedBox(height: 5),
        IntrinsicHeight(
          child: Row(
            children: [
              Expanded(
                child: FormBuilderTextField(
                  name: 'nm_customer',
                  controller: namaController,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    isDense: true,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 15, vertical: 13),
                    hintText: 'Cth: Agus Supriyatno',
                    hintStyle: TextStyle(fontSize: 14),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: sRadius5,
                        bottomLeft: sRadius5,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: sRadius5,
                        bottomLeft: sRadius5,
                      ),
                      borderSide: BorderSide(color: cBlack10),
                    ),
                  ),
                ),
              ),
              CupertinoButton(
                onPressed: () {
                  return Get.to(
                    CustomerPage(tipe: 'select'),
                    preventDuplicates: false,
                  ).then(
                    (value) {
                      if (value != null)
                        PosController().setCustomer(
                          namaController,
                          alamatController,
                          teleponController,
                          value,
                        );
                    },
                  );
                },
                minSize: 5,
                padding: sNoPadding,
                child: Container(
                  height: double.infinity,
                  padding: sPaddingHor15,
                  decoration: BoxDecoration(
                    color: cBlue35,
                    borderRadius: BorderRadius.only(
                      topRight: sRadius5,
                      bottomRight: sRadius5,
                    ),
                  ),
                  child: Icon(Icons.search, size: 18, color: cWhite),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

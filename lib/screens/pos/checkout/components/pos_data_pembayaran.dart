import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/pos/checkout/checkout.dart';
import 'package:redpos/screens/pos/checkout/components/checkout_item.dart';
import 'package:redpos/widgets/line_title.dart';

class PosDataPembayaran extends StatelessWidget {
  const PosDataPembayaran({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final CheckoutPage widget;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Pembayaran',
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
        SizedBox(height: 10),
        LineTitle(width: Get.width * .1),
        SizedBox(height: 15),
        CheckoutItem(
          title: 'Total',
          posController: widget.posController,
          value: widget.posController.totalCart,
        ),
        CheckoutItem(
          title: 'Biaya Tambahan',
          posController: widget.posController,
          value: widget.posController.biayaTambahan,
          tipe: 'form',
        ),
        CheckoutItem(
          title: 'Disc',
          posController: widget.posController,
          value: widget.posController.disc,
          tipe: 'form',
        ),
        GetBuilder<PosController>(
          builder: (_) {
            return CheckoutItem(
              title: 'Ballance',
              posController: widget.posController,
              value: widget.posController.balance,
            );
          },
        ),
        Divider(),
        CartCheckoutSelect(
          title: 'Pembayaran',
          posController: widget.posController,
          items: pembayaranList,
        ),
        CheckoutItem(
          title: 'Terbayar',
          posController: widget.posController,
          tipe: 'form',
        ),
      ],
    );
  }
}

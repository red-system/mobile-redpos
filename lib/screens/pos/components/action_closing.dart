import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/screens/pos/components/pos_closing.dart';

class ActionClosing extends StatelessWidget {
  const ActionClosing({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: () => Get.bottomSheet(PosClosing()),
      padding: EdgeInsets.only(right: 15),
      minSize: 5,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: cBlue40,
          borderRadius: sBorderRadius5,
        ),
        child: Row(
          children: [
            Icon(
              Icons.close,
              color: cWhite,
              size: 16,
            ),
            SizedBox(width: 5),
            Text(
              'Closing',
              style: TextStyle(
                fontSize: 12,
                color: cWhite,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

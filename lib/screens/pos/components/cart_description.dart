import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:redpos/screens/pos/components/cart_kuantiti_set.dart';

class CartDescription extends StatelessWidget {
  const CartDescription({
    Key key,
    @required this.result,
    @required this.index,
    @required this.posController,
  }) : super(key: key);

  final CartResponse result;
  final int index;
  final PosController posController;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: IntrinsicHeight(
        child: Row(
          children: [
            Container(
              padding: sPaddingAll10,
              width: Get.width * .20,
              height: Get.width * .20,
              decoration: BoxDecoration(
                color: cGrey30,
                borderRadius: sBorderRadius15,
              ),
            ),
            SizedBox(width: 20),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 5),
                    Text(
                      result.nama,
                      style: TextStyle(
                        color: cBlack80,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 3),
                    Text(
                      result.kodeProduk,
                      style: TextStyle(
                        color: cBlack80,
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    Spacer(),
                    Text.rich(
                      TextSpan(
                        text: "Rp ${money.format(result.harga)}",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: cBlue35,
                        ),
                        children: [
                          TextSpan(
                            text: "  x${money.format(result.kuantiti)}",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              color: cBlack80,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                  ],
                ),
              ),
            ),
            SizedBox(width: 5),
            CartKuantitiSet(
              posController: posController,
              result: result,
              index: index,
            )
          ],
        ),
      ),
    );
  }
}

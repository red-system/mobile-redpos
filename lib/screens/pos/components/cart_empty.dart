import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/style.dart';

class CartEmpty extends StatelessWidget {
  const CartEmpty({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: sPaddingHor15,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/cart-empty.png',
            width: Get.width * .6,
          ),
          SizedBox(height: 10),
          Text('Cart Kosong', style: TextStyle(fontSize: 16)),
        ],
      ),
    );
  }
}

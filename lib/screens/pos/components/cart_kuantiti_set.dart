import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/models/cart_response.dart';

class CartKuantitiSet extends StatelessWidget {
  const CartKuantitiSet({
    Key key,
    @required this.posController,
    @required this.index,
    @required this.result,
  }) : super(key: key);

  final PosController posController;
  final int index;
  final CartResponse result;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: sPaddingAll10,
      child: Column(
        children: [
          CupertinoButton(
            onPressed: () => posController.setKuantiti('plus', index, result),
            padding: sNoPadding,
            minSize: 5,
            child: Container(
              padding: sPaddingAll5,
              decoration: BoxDecoration(
                color: cBlue35,
                borderRadius: sBorderRadius5,
              ),
              child: Icon(Icons.add, size: 14, color: cWhite),
            ),
          ),
          SizedBox(height: 7),
          CupertinoButton(
            onPressed: () => posController.setKuantiti('min', index, result),
            padding: sNoPadding,
            minSize: 5,
            child: Container(
              padding: sPaddingAll5,
              decoration: BoxDecoration(
                color: cGrey30,
                borderRadius: sBorderRadius5,
              ),
              child: Icon(Icons.remove, size: 14, color: cBlack80),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/pos/components/cart_empty.dart';
import 'package:redpos/screens/pos/components/produk_list.dart';

class CartProduk extends StatelessWidget {
  const CartProduk({
    Key key,
    @required this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosController>(
      builder: (_) {
        return Expanded(
          child: cartList.length == 0
              ? CartEmpty()
              : ProdukList(posController: posController),
        );
      },
    );
  }
}

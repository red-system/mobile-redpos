import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/models/cart_response.dart';
import 'package:redpos/screens/pos/components/cart_description.dart';
import 'package:redpos/screens/pos/components/dialog_trash.dart';

class CartProdukItem extends StatelessWidget {
  const CartProdukItem({
    Key key,
    @required this.result,
    @required this.index,
    @required this.posController,
  }) : super(key: key);

  final CartResponse result;
  final int index;
  final PosController posController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        index == 0 ? SizedBox() : Divider(),
        Padding(
          padding: sPaddingVer5,
          child: Dismissible(
            key: Key(result.idCart.toString()),
            direction: DismissDirection.endToStart,
            confirmDismiss: (direction) async {
              return showModalBottomSheet(
                context: context,
                builder: (_) => DialogTrash(),
              ).then((value) async {
                if (value == null) {
                  return false;
                } else {
                  posController.removeCart(result);
                  return true;
                }
              });
            },
            background: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.redAccent,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Spacer(),
                  Icon(Icons.delete, color: cWhite),
                ],
              ),
            ),
            child: CartDescription(
              posController: posController,
              result: result,
              index: index,
            ),
          ),
        ),
      ],
    );
  }
}

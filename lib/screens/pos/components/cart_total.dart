import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/pos/checkout/checkout.dart';

class CartTotal extends StatelessWidget {
  const CartTotal({
    Key key,
    @required this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosController>(
      builder: (_) {
        return cartList.length == 0
            ? SizedBox()
            : Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                margin: sPaddingHor15,
                decoration: BoxDecoration(
                  color: cWhite,
                  boxShadow: [sShadowBox],
                  borderRadius: sBorderRadius5,
                ),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Total',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.w300),
                          ),
                          SizedBox(height: 3),
                          Text(
                            'Rp ${money.format(_.totalCart)}',
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    CupertinoButton(
                      onPressed: () async {
                        posController.getTotalCheckOut('', '');
                        Get.to(
                          CheckoutPage(posController: posController),
                        ).then((value) => posController.resetCheckOut());
                      },
                      padding: sNoPadding,
                      minSize: 5,
                      color: cBlue35,
                      borderRadius: sBorderRadius5,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 10,
                        ),
                        child: Text('Chekout', style: TextStyle(fontSize: 14)),
                      ),
                    )
                  ],
                ),
              );
      },
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';


class DialogTrash extends StatelessWidget {
  const DialogTrash({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: sPaddingAll20,
      decoration: BoxDecoration(
        color: cWhite,
        borderRadius: sBorderRadius20,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Apakah anda yakin ingin menghapus ?',
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20),
          Image.asset('assets/images/trash.png', width: Get.width * .7),
          SizedBox(height: 30),
          IntrinsicHeight(
            child: Row(
              children: [
                Expanded(
                  child: CupertinoButton(
                    minSize: 5,
                    onPressed: () => Get.back(),
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 5,
                    ),
                    color: cGrey30,
                    borderRadius: sBorderRadius5,
                    child: Text(
                      'Tidak',
                      style: TextStyle(
                        fontSize: 14,
                        color: cBlack80,
                        letterSpacing: 0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                SizedBox(width: 5),
                Expanded(
                  child: CupertinoButton(
                    minSize: 5,
                    onPressed: () {
                      Get.back(result: true);
                    },
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 7,
                    ),
                    color: Colors.green,
                    borderRadius: sBorderRadius5,
                    child: Text(
                      'Iya',
                      style: TextStyle(
                        fontSize: 14,
                        letterSpacing: 0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}

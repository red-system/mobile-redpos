import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/home/home_controller.dart';
import 'package:redpos/screens/home/home.dart';
import 'package:redpos/screens/print/print.dart';
import 'package:redpos/widgets/line_title.dart';

class PosClosing extends StatelessWidget {
  const PosClosing({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: sPaddingAll15,
      margin: EdgeInsets.only(left: 15, right: 15, bottom: 20),
      decoration: BoxDecoration(color: cWhite, borderRadius: sBorderRadius7),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Closing POS',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: cBlack80,
            ),
          ),
          SizedBox(height: 7),
          LineTitle(width: Get.width * .1),
          SizedBox(height: 20),
          PosClosingItem(title: 'Staff', value: sess.read('namaUser')),
          PosClosingItem(title: 'Waktu Buka', value: sess.read('openDate')),
          PosClosingItem(
            title: 'Jumlah Transaksi',
            value: money.format(sess.read('jumlahTransaksi')),
          ),
          PosClosingItem(
            title: 'Total Transaksi',
            value: 'Rp. ${money.format(sess.read('totalTransaksi'))}',
          ),
          PosClosingItem(
            title: 'Kas Awal',
            value: 'Rp. ${money.format(sess.read('kasAwal'))}',
          ),
          PosClosingItem(
            title: 'Kas Akhir',
            value: 'Rp. ${money.format(sess.read('kasAkhir'))}',
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: () => Get.back(),
                style: ElevatedButton.styleFrom(primary: cBlack10),
                child: Text(
                  'Cancel',
                  style: TextStyle(fontSize: 12, color: cBlack80),
                ),
              ),
              SizedBox(width: 10),
              ElevatedButton(
                onPressed: () {
                  Get.offAll(HomePage());
                  Get.to(PrintPage(transaksi: '', bagian: 'closing'));
                },
                style: ElevatedButton.styleFrom(primary: cBlue35),
                child: Text('Closing', style: TextStyle(fontSize: 12)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class PosClosingItem extends StatelessWidget {
  const PosClosingItem({
    Key key,
    @required this.title,
    @required this.value,
  }) : super(key: key);

  final String title, value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: Get.width * .35,
            child: Text(title),
          ),
          Text(' : '),
          Expanded(
            child: Text(value),
          )
        ],
      ),
    );
  }
}

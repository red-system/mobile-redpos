import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:intl/intl.dart';

class PosPrint extends StatelessWidget {
  const PosPrint({
    Key key,
    @required this.transaksi,
    @required this.noFaktur,
  }) : super(key: key);

  final transaksi;
  final String noFaktur;

  @override
  Widget build(BuildContext context) {
    final dtCustomer = transaksi['customer'];
    final dtCart = transaksi['transaksi_detail'];
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    return Container(
      decoration: BoxDecoration(
        color: cWhite,
        borderRadius: BorderRadius.only(
          topLeft: sRadius10,
          topRight: sRadius10,
        ),
      ),
      child: Padding(
        padding: sPaddingAll15,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.center,
              child: Text(
                'Redpos Resto',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: cBlack80,
                ),
              ),
            ),
            SizedBox(height: 40),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .2,
                  child: Text('Code'),
                ),
                Text(' : '),
                Expanded(child: Text(noFaktur)),
              ],
            ),
            SizedBox(height: 5),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .2,
                  child: Text('Date'),
                ),
                Text(' : '),
                Expanded(child: Text(formatter.format(DateTime.now()))),
              ],
            ),
            SizedBox(height: 5),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .2,
                  child: Text('Customer'),
                ),
                Text(' : '),
                Expanded(child: Text(dtCustomer['nm_customer'])),
              ],
            ),
            SizedBox(height: 5),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .2,
                  child: Text('Staff'),
                ),
                Text(' : '),
                Expanded(child: Text(sess.read('namaUser'))),
              ],
            ),
            Divider(height: 30),
            Column(
              children: dtCart.map<Widget>((e) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        e['nama'],
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: cBlack80,
                        ),
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          SizedBox(
                            width: Get.width * .2,
                            child: Text('${money.format(e['kuantiti'])} pcs'),
                          ),
                          SizedBox(width: 5),
                          SizedBox(
                            width: Get.width * .2,
                            child: Text('x ${money.format(e['harga'])}'),
                          ),
                          SizedBox(width: 5),
                          Expanded(
                            child: Text(
                              money.format(e['kuantiti'] * e['harga']),
                              style: TextStyle(fontWeight: FontWeight.bold),
                              textAlign: TextAlign.end,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                );
              }).toList(),
            ),
            Divider(height: 30),
            transaksi['biaya_tambahan'] == 0
                ? SizedBox()
                : Padding(
                    padding: const EdgeInsets.only(bottom: 7),
                    child: Row(
                      children: [
                        SizedBox(
                          width: Get.width * .6,
                          child: Text(
                            'Additional Cost',
                            style: TextStyle(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.end,
                          ),
                        ),
                        Text(' : '),
                        Expanded(
                          child: Text(
                            money.format(transaksi['biaya_tambahan']),
                            style: TextStyle(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.end,
                          ),
                        )
                      ],
                    ),
                  ),
            transaksi['disc'] == 0
                ? SizedBox()
                : Padding(
                    padding: const EdgeInsets.only(bottom: 7),
                    child: Row(
                      children: [
                        SizedBox(
                          width: Get.width * .6,
                          child: Text(
                            'Disc',
                            style: TextStyle(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.end,
                          ),
                        ),
                        Text(' : '),
                        Expanded(
                          child: Text(
                            money.format(transaksi['disc']),
                            style: TextStyle(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.end,
                          ),
                        )
                      ],
                    ),
                  ),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .6,
                  child: Text(
                    'Total Price',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.end,
                  ),
                ),
                Text(' : '),
                Expanded(
                  child: Text(
                    money.format(transaksi['balance']),
                    style: TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.end,
                  ),
                )
              ],
            ),
            SizedBox(height: 7),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .6,
                  child: Text(
                    'Amount Received',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.end,
                  ),
                ),
                Text(' : '),
                Expanded(
                  child: Text(
                    money.format(transaksi['terbayar']),
                    style: TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.end,
                  ),
                )
              ],
            ),
            SizedBox(height: 7),
            Row(
              children: [
                SizedBox(
                  width: Get.width * .6,
                  child: Text(
                    'Changes',
                    style: TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.end,
                  ),
                ),
                Text(' : '),
                Expanded(
                  child: Text(
                    money.format(transaksi['sisa']),
                    style: TextStyle(fontWeight: FontWeight.bold),
                    textAlign: TextAlign.end,
                  ),
                )
              ],
            ),
            SizedBox(height: 25),
            Align(
              alignment: Alignment.center,
              child: Text(
                'Thank you for visiting us',
                style: TextStyle(fontWeight: FontWeight.w300),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}

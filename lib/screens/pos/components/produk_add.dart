import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/controllers/pos/produk_controller.dart';
import 'package:redpos/screens/pos/components/produk_item_list.dart';
import 'package:redpos/screens/pos/components/produk_item_search.dart';
import 'package:redpos/screens/pos/components/produk_select.dart';

class ProdukAdd extends StatefulWidget {
  const ProdukAdd({
    Key key,
    @required this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  _ProdukAddState createState() => _ProdukAddState();
}

class _ProdukAddState extends State<ProdukAdd> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: sPaddingHor15,
      child: CupertinoButton(
        onPressed: () async {
          await Get.bottomSheet(
            ProdukSelect(posController: widget.posController),
          ).then((data) {
            if (data != null) widget.posController.addToCart(data);
          });
        },
        padding: sNoPadding,
        child: DottedBorder(
          padding: sPaddingAll10,
          color: cBlack20,
          borderType: BorderType.RRect,
          radius: sRadius5,
          child: SizedBox(
            width: double.infinity,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: sPaddingAll10,
                  decoration: BoxDecoration(
                    color: cWhite,
                    boxShadow: [sShadow],
                    borderRadius: sBorderRadius5,
                  ),
                  child: Icon(Icons.add, color: cBlue35),
                ),
                SizedBox(width: 20),
                Text(
                  'Tambah Produk',
                  style: TextStyle(
                    color: cBlack80,
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    letterSpacing: 0,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

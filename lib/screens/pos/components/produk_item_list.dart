import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/models/produk_response.dart';

class ProdukItemList extends StatelessWidget {
  const ProdukItemList({
    Key key,
    @required this.index,
    @required this.data,
    @required this.posController,
  }) : super(key: key);

  final int index;
  final ProdukResponse data;
  final PosController posController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        index == 0 ? SizedBox() : Divider(height: 20),
        Container(
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.nama,
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 5),
                    Text(data.kodeBarang, style: TextStyle(fontSize: 14)),
                  ],
                ),
              ),
              SizedBox(width: 10),
              CupertinoButton(
                onPressed: () => Get.back(result: data),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                color: Colors.green,
                borderRadius: sBorderRadius5,
                minSize: 5,
                child: Container(
                  child: Row(
                    children: [
                      Icon(Icons.check, size: 18),
                      SizedBox(width: 5),
                      Text('Pilih', style: TextStyle(fontSize: 12))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';

class ProdukItemSearch extends StatefulWidget {
  const ProdukItemSearch({
    Key key,
    @required this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  _ProdukItemSearchState createState() => _ProdukItemSearchState();
}

class _ProdukItemSearchState extends State<ProdukItemSearch> {
  TextEditingController _searchController = TextEditingController();

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosController>(builder: (_) {
      return Padding(
        padding: sPaddingHor15,
        child: FormBuilderTextField(
          name: 'nm_searchproduk',
          controller: _searchController,
          style: TextStyle(fontSize: 12),
          onChanged: (text) => widget.posController.searchProduk(text),
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 13,
            ),
            hintText: 'Search Produk',
            hintStyle: TextStyle(fontSize: 12),
            prefixIcon: Padding(
              padding: sPaddingHor15,
              child: Icon(Icons.search),
            ),
            suffixIcon: _.showClearSearch
                ? Padding(
                    padding: sPaddingHor15,
                    child: CupertinoButton(
                      onPressed: () => widget.posController
                          .clearSearchForm(_searchController),
                      padding: sNoPadding,
                      minSize: 5,
                      child:
                          Icon(Icons.close, size: 16, color: Colors.redAccent),
                    ),
                  )
                : SizedBox(),
            prefixIconConstraints: BoxConstraints(minWidth: 0),
            suffixIconConstraints: BoxConstraints(minWidth: 0),
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(
              borderRadius: sBorderRadius5,
              borderSide: BorderSide(
                color: cBlack20,
              ),
            ),
          ),
        ),
      );
    });
  }
}

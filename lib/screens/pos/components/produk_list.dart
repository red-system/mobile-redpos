import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/pos/components/cart_produk_item.dart';

class ProdukList extends StatelessWidget {
  const ProdukList({
    Key key,
    @required this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: sPaddingHor15,
          child: Text(
            'List Cart',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: cBlack80,
            ),
          ),
        ),
        SizedBox(height: 10),
        Expanded(
          child: ListView.builder(
            itemCount: cartList.length,
            padding: sPaddingHor15,
            itemBuilder: (context, index) => CartProdukItem(
              index: index,
              result: cartList[index],
              posController: posController,
            ),
          ),
        ),
      ],
    );
  }
}

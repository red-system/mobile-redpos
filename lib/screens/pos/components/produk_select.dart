import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/core/controllers/pos/produk_controller.dart';
import 'package:redpos/screens/pos/components/produk_item_list.dart';
import 'package:redpos/screens/pos/components/produk_item_search.dart';
import 'package:redpos/widgets/loading_proses_custom.dart';

class ProdukSelect extends StatefulWidget {
  const ProdukSelect({
    Key key,
    @required this.posController,
  }) : super(key: key);

  final PosController posController;

  @override
  _ProdukSelectState createState() => _ProdukSelectState();
}

class _ProdukSelectState extends State<ProdukSelect> {
  ProdukController _produkController = Get.put(ProdukController());

  @override
  void initState() {
    _produkController.getProduk();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => focusScope(context),
      child: Container(
        height: Get.height * .6,
        decoration: BoxDecoration(
          color: cWhite,
          borderRadius: BorderRadius.only(
            topLeft: sRadius20,
            topRight: sRadius20,
          ),
        ),
        child: Stack(
          children: [
            SizedBox(
              width: double.infinity,
              height: Get.height * .6,
            ),
            _produkController.obx(
              (state) {
                return Column(
                  children: [
                    SizedBox(height: 20),
                    ProdukItemSearch(posController: widget.posController),
                    SizedBox(height: 5),
                    Expanded(
                      child: GetBuilder<PosController>(builder: (_) {
                        return ListView.builder(
                          scrollDirection: Axis.vertical,
                          padding: sPaddingAll15,
                          itemCount: produkListSearch.length,
                          itemBuilder: (_, index) => ProdukItemList(
                            index: index,
                            data: produkListSearch[index],
                            posController: widget.posController,
                          ),
                        );
                      }),
                    )
                  ],
                );
              },
              onLoading: CustomLoadingProses(),
              onEmpty: SizedBox(),
              onError: (err) {
                print(err.toString());
                return CupertinoButton(
                  onPressed: () => _produkController.getProduk(),
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.refresh_rounded,
                          size: Get.width * .2,
                          color: cBlack40,
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Data not response.',
                          style: TextStyle(fontSize: 14, color: cBlack60),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

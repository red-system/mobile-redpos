import 'package:dotted_border/dotted_border.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/pos/pos_controller.dart';
import 'package:redpos/screens/pos/components/action_closing.dart';
import 'package:redpos/screens/pos/components/cart_produk.dart';
import 'package:redpos/screens/pos/components/cart_total.dart';
import 'package:redpos/screens/pos/checkout/components/pos_data_customer.dart';
import 'package:redpos/screens/pos/components/produk_add.dart';

class PosPage extends StatefulWidget {
  const PosPage({Key key}) : super(key: key);

  @override
  _PosPageState createState() => _PosPageState();
}

class _PosPageState extends State<PosPage> {
  final GlobalKey<FormBuilderState> formSignInBuilderKey =
      GlobalKey<FormBuilderState>();

  PosController _posController = Get.put(PosController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => focusScope(context),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Pos', style: TextStyle(color: cWhite)),
          brightness: Brightness.dark,
          iconTheme: IconThemeData(color: cWhite),
          actions: [ActionClosing()],
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              ProdukAdd(posController: _posController),
              SizedBox(height: 10),
              Padding(padding: sPaddingHor15, child: Divider()),
              SizedBox(height: 10),
              CartProduk(posController: _posController),
              SizedBox(height: 5),
              CartTotal(posController: _posController),
              SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }
}

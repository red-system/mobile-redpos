import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter_bluetooth_basic/flutter_bluetooth_basic.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'dart:io' show Platform;
import 'package:intl/intl.dart';
import 'package:redpos/core/controllers/home/home_controller.dart';

import 'package:redpos/core/models/cart_response.dart';

class PrintPage extends StatefulWidget {
  const PrintPage({
    Key key,
    @required this.transaksi,
    this.noFaktur = '-',
    @required this.bagian,
  }) : super(key: key);

  final transaksi;
  final String noFaktur, bagian;

  @override
  _PrintPageState createState() => _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  PrinterBluetoothManager _printerManager = PrinterBluetoothManager();
  List<PrinterBluetooth> _devices = [];
  String _devicesMsg;
  BluetoothManager bluetoothManager = BluetoothManager.instance;

  bool checkBagian = false;

  @override
  void initState() {
    setInit();
    super.initState();
  }

  setInit() {
    setState(() {
      checkBagian = widget.bagian == 'checkout'
          ? true
          : (widget.bagian == 'closing' ? true : false);

      if (checkBagian) {
        if (Platform.isAndroid) {
          bluetoothManager.state.listen((val) {
            print('state = $val');
            if (!mounted) return;
            if (val == 12) {
              print('on');
              initPrinter();
            } else if (val == 10) {
              print('off');
              setState(() => _devicesMsg = 'Bluetooth Disconnect!');
            }
          });
        } else {
          initPrinter();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Print'),
        ),
        body: checkBagian
            ? _devices.isEmpty
                ? CupertinoButton(
                    onPressed: () => setInit(),
                    child: Container(
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.refresh_rounded, size: Get.width * .15),
                          SizedBox(height: 5),
                          Text(
                            _devicesMsg ?? '',
                            style: TextStyle(fontSize: 16, color: cBlack60),
                          ),
                        ],
                      ),
                    ),
                  )
                : ListView.builder(
                    itemCount: _devices.length,
                    itemBuilder: (c, i) {
                      return ListTile(
                        leading: Icon(Icons.print),
                        title: Text(_devices[i].name),
                        subtitle: Text(_devices[i].address),
                        onTap: () {
                          _startPrint(_devices[i]);
                        },
                      );
                    },
                  )
            : Center(child: Text('Bagian yang diprint tidak ditemukan.')));
  }

  void initPrinter() {
    _printerManager.startScan(Duration(seconds: 2));
    _printerManager.scanResults.listen((val) {
      if (!mounted) return;
      setState(() => _devices = val);
      if (_devices.isEmpty) setState(() => _devicesMsg = 'No Devices');
    });
  }

  Future<void> _startPrint(PrinterBluetooth printer) async {
    String message = '-';
    _printerManager.selectPrinter(printer);

    if (widget.bagian == 'checkout') {
      final result = await _printerManager
          .printTicket(await _ticketCheckout(PaperSize.mm58));
      message = result.msg;
      if (message == 'Success') {
        Get.back();
      }
    }
    if (widget.bagian == 'closing') {
      final result = await _printerManager
          .printTicket(await _ticketClosing(PaperSize.mm58));
      message = result.msg;
      if (message == 'Success') {
        HomeController().updClosing();
        Get.back();
      }
    }

    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        content: Text(message),
      ),
    );
  }

  Future<Ticket> _ticketCheckout(PaperSize paper) async {
    final dtCustomer = widget.transaksi['customer'];
    final dtCart = widget.transaksi['transaksi_detail'];

    final ticket = Ticket(paper);

    ticket.text(
      'Redpos Resto',
      styles: PosStyles(
        align: PosAlign.center,
        height: PosTextSize.size2,
        width: PosTextSize.size2,
      ),
      linesAfter: 1,
    );
    ticket.feed(1);
    ticket.row([
      PosColumn(text: 'Code', width: 4),
      PosColumn(text: ' : ${widget.noFaktur}', width: 8),
    ]);
    ticket.row([
      PosColumn(text: 'Date', width: 4),
      PosColumn(text: ' : ${formatter.format(DateTime.now())}', width: 8),
    ]);
    ticket.row([
      PosColumn(text: 'Customer', width: 4),
      PosColumn(text: ' : ${dtCustomer['nm_customer']}', width: 8),
    ]);
    ticket.row([
      PosColumn(text: 'Staff', width: 4),
      PosColumn(text: ' : ${sess.read('namaUser')}', width: 8),
    ]);
    ticket.feed(1);

    for (var i = 0; i < dtCart.length; i++) {
      final res = dtCart[i];
      ticket.text(res['nama'], styles: PosStyles(bold: true));
      ticket.row([
        PosColumn(
          text: '${money.format(res['kuantiti'])} pcs',
          width: 3,
          styles: PosStyles(
            align: PosAlign.left,
          ),
        ),
        PosColumn(
          text: 'x ${money.format(res['harga'])}',
          width: 4,
          styles: PosStyles(
            align: PosAlign.right,
          ),
        ),
        PosColumn(
          text: '${money.format(res['kuantiti'] * res['harga'])}',
          width: 5,
          styles: PosStyles(
            align: PosAlign.right,
          ),
        ),
      ]);
    }

    ticket.feed(1);
    ticket.row([
      PosColumn(
        text: 'Total Price :',
        width: 7,
        styles: PosStyles(bold: true, align: PosAlign.right),
      ),
      PosColumn(
        text: money.format(widget.transaksi['balance']),
        width: 5,
        styles: PosStyles(bold: true, align: PosAlign.right),
      ),
    ]);
    ticket.row([
      PosColumn(
        text: 'Amount Received :',
        width: 7,
        styles: PosStyles(bold: true, align: PosAlign.right),
      ),
      PosColumn(
        text: money.format(widget.transaksi['terbayar']),
        width: 5,
        styles: PosStyles(bold: true, align: PosAlign.right),
      ),
    ]);
    ticket.row([
      PosColumn(
        text: 'Changes :',
        width: 7,
        styles: PosStyles(bold: true, align: PosAlign.right),
      ),
      PosColumn(
        text: money.format(widget.transaksi['sisa']),
        width: 5,
        styles: PosStyles(bold: true, align: PosAlign.right),
      ),
    ]);

    ticket.feed(2);
    ticket.text(
      'Thank you for visiting us',
      styles: PosStyles(align: PosAlign.center),
    );
    ticket.cut();

    return ticket;
  }

  Future<Ticket> _ticketClosing(PaperSize paper) async {
    final ticket = Ticket(paper);

    ticket.text(
      'Redpos Resto',
      styles: PosStyles(
        align: PosAlign.center,
        height: PosTextSize.size2,
        width: PosTextSize.size2,
      ),
    );

    ticket.text(
      'Info Closing',
      styles: PosStyles(align: PosAlign.center),
      linesAfter: 1,
    );
    ticket.feed(1);

    ticket.row([
      PosColumn(text: 'Staff', width: 6),
      PosColumn(text: ' : ${sess.read('namaUser')}', width: 6),
    ]);
    ticket.row([
      PosColumn(text: 'Waktu Buka', width: 6),
      PosColumn(
          text: ' : ${datetime.format(DateTime.parse(sess.read('openDate')))}',
          width: 6),
    ]);
    ticket.row([
      PosColumn(text: 'Waktu Tutup', width: 6),
      PosColumn(text: ' : ${datetime.format(DateTime.now())}', width: 6),
    ]);
    ticket.row([
      PosColumn(text: 'Jumlah Transaksi', width: 6),
      PosColumn(
          text: ' : ${money.format(sess.read('jumlahTransaksi'))}', width: 6),
    ]);
    ticket.row([
      PosColumn(text: 'Total Transaksi', width: 6),
      PosColumn(
          text: ' : ${money.format(sess.read('totalTransaksi'))}', width: 6),
    ]);
    ticket.row([
      PosColumn(text: 'Kas Awal', width: 6),
      PosColumn(text: ' : ${money.format(sess.read('kasAwal'))}', width: 6),
    ]);
    ticket.row([
      PosColumn(text: 'Kas Akhir', width: 6),
      PosColumn(text: ' : ${money.format(sess.read('kasAkhir'))}', width: 6),
    ]);

    ticket.cut();

    return ticket;
  }

  @override
  void dispose() {
    _printerManager.stopScan();
    super.dispose();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/constants/url.dart';
import 'package:redpos/core/controllers/sign/signin_controller.dart';
import 'package:redpos/core/controllers/sign/toggle_password_controller.dart';
import 'package:redpos/functions/func_signin.dart';
import 'package:redpos/widgets/button_default.dart';
import 'package:redpos/widgets/sign_form_password.dart';
import 'package:redpos/widgets/sign_form_text.dart';

class FormSignIn extends StatefulWidget {
  FormSignIn({
    Key key,
    @required this.formSignBuilderKey,
  }) : super(key: key);

  final GlobalKey<FormBuilderState> formSignBuilderKey;

  @override
  _FormSignInState createState() => _FormSignInState();
}

class _FormSignInState extends State<FormSignIn> {
  SignInController _signInController = Get.put(SignInController());
  TogglePasswordController _togglePinController =
      Get.put(TogglePasswordController());

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: widget.formSignBuilderKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 30),
          Container(
            padding: sPaddingHor20,
            alignment: Alignment.center,
            child: Text(
              'Sign In',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: cBlack80,
              ),
            ),
          ),
          SizedBox(height: 10),
          Align(
            alignment: Alignment.center,
            child: Container(
              width: Get.width * .1,
              height: 2,
              decoration: BoxDecoration(
                color: cBlue35,
                borderRadius: sBorderRadius10,
              ),
            ),
          ),
          SizedBox(height: 30),
          SignFormText(
            name: 'nm_username',
            title: 'Username',
            tipe: 'text',
            prefix: LineIcons.user,
          ),
          SizedBox(height: 15),
          SignFormPassword(
            name: 'nm_password',
            title: 'Password',
            tipe: 'text',
            prefix: LineIcons.key,
          ),
          SizedBox(height: 25),
          ButtonDefault(
            press: () {
              final check = checkFormSignIn(widget.formSignBuilderKey);
              if (check != null) _signInController.toSignIn(uLogin, check);
            },
            text: 'Sign In',
          ),
          SizedBox(height: 30),
        ],
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/routes.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/sign/signin_controller.dart';
import 'package:redpos/screens/signin/components/form_signin.dart';
import 'package:redpos/widgets/loading_proses_custom.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  SignInController _signInController = Get.put(SignInController());

  final GlobalKey<FormBuilderState> formSignInBuilderKey =
      GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: GestureDetector(
        onTap: () => focusScope(context),
        child: Scaffold(
          body: Stack(
            children: [
              FormSignIn(
                formSignBuilderKey: formSignInBuilderKey,
              ),
              _signInController.obx(
                (state) => SizedBox(),
                onEmpty: SizedBox(),
                onError: (e) {
                  print(e.toString());
                  return SizedBox();
                },
                onLoading: CustomLoadingProses(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

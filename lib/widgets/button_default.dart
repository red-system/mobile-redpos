import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';

class ButtonDefault extends StatelessWidget {
  const ButtonDefault({
    Key key,
    @required this.press,
    @required this.text,
    this.colorButton = cBlue35,
    this.colorButtonText = cWhite,
    this.colorBorder = cTransparant,
    this.elevation = 1,
  }) : super(key: key);

  final Function press;
  final String text;
  final double elevation;
  final colorButton, colorButtonText, colorBorder;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: sPaddingHor20,
      child: RaisedButton(
        onPressed: press,
        color: colorButton,
        elevation: elevation,
        shape: RoundedRectangleBorder(
            borderRadius: sBorderRadius5,
            side: BorderSide(color: colorBorder, width: .4)),
        child: Text(text, style: TextStyle(color: colorButtonText)),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:redpos/constants/size_config.dart';

class DataNotFound extends StatelessWidget {
  const DataNotFound({
    Key key,
    @required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: Text(
          text,
          style: TextStyle(fontSize: 16),
        ),
      ),
    );
  }
}

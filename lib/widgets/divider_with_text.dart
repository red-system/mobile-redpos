import 'package:flutter/material.dart';

class DividerWithText extends StatelessWidget {
  const DividerWithText({
    Key key, this.text, this.colorText, this.colorDivider = Colors.grey, this.padd = 0,
  }) : super(key: key);

  final String text;
  final Color colorText, colorDivider;
  final double padd;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: padd),
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Divider(color: colorDivider, height: 1,),
            ),
          ),
          Text(text, style: TextStyle(fontSize: 12,color: colorText),),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Divider(color: colorDivider, height: 1,),
            ),
          ),
        ],
      ),
    );
  }
}
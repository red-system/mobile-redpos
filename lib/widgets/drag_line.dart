import 'package:flutter/material.dart';
import 'package:redpos/constants/size_config.dart';

class DragLine extends StatelessWidget {
  const DragLine({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        height: 3,
        width: screenWidth(context) * .2,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.grey[700], borderRadius: BorderRadius.circular(10)),
      ),
    );
  }
}

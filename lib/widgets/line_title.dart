import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';

class LineTitle extends StatelessWidget {
  const LineTitle({
    Key key,
    @required this.width,
    this.height = 2,
    this.color = cBlue35,
  }) : super(key: key);

  final double width, height;
  final color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: color,
        borderRadius: sBorderRadius10,
      ),
    );
  }
}

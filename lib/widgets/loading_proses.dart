import 'package:flutter/material.dart';


class LoadingProses extends StatelessWidget {
  const LoadingProses({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white.withOpacity(.3),
        alignment: Alignment.center,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 3
              )
            ]
          ),
          child: CircleAvatar(
            backgroundColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.grey[700])),
            )
          ),
        ),
      );
  }
}
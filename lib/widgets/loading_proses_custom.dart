import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';

class CustomLoadingProses extends StatelessWidget {
  const CustomLoadingProses({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: cBlue15.withOpacity(.05),
      height: Get.height,
      width: Get.width,
      alignment: sAlignmentC,
      child: SpinKitThreeBounce(color: cBlue35, size: 28),
    );
  }
}

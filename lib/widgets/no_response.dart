import 'package:flutter/material.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/size_config.dart';
import 'package:redpos/constants/style.dart';

class NoResponse extends StatelessWidget {
  const NoResponse({
    Key key,
    @required this.press,
  }) : super(key: key);

  final Function press;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white.withOpacity(.3),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Sistem Not Response',
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.w300, color: cBlack80),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20),
          SizedBox(
            width: screenWidth(context) * .4,
            child: RaisedButton(
              onPressed: press,
              color: Colors.green,
              shape: RoundedRectangleBorder(borderRadius: sBorderRadius7),
              child: Text('Reload', style: TextStyle(color: cWhite)),
            ),
          )
        ],
      ),
    );
  }
}

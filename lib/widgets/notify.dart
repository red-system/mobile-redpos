import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:redpos/constants/color.dart';

class Notify {
  final String message;
  final color;

  Notify({this.message = '', this.color = cBlack10}) {
    Get.snackbar(
      'Pesan :',
      message,
      backgroundColor: color,
      colorText: cWhite,
      duration: 3.seconds,
      snackPosition: SnackPosition.BOTTOM,
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/init.dart';
import 'package:redpos/constants/style.dart';
import 'package:redpos/core/controllers/sign/toggle_password_controller.dart';

class SignFormPassword extends StatefulWidget {
  const SignFormPassword({
    Key key,
    @required this.name,
    @required this.title,
    @required this.tipe,
    @required this.prefix,
    this.margin = 20,
  }) : super(key: key);

  final String name, title, tipe;
  final double margin;
  final prefix;

  @override
  _SignFormPasswordState createState() => _SignFormPasswordState();
}

class _SignFormPasswordState extends State<SignFormPassword> {
  TogglePasswordController _togglePinController =
      Get.put(TogglePasswordController());

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: widget.margin),
      child: GetBuilder<TogglePasswordController>(
        builder: (_) {
          return FormBuilderTextField(
            name: widget.name,
            style: TextStyle(fontSize: 14),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(context,
                  errorText: 'Kolom tidak boleh kosong.'),
            ]),
            decoration: InputDecoration(
              hintText: 'Input ${widget.title}',
              hintStyle: TextStyle(fontSize: 14),
              isDense: true,
              contentPadding: sNoPadding,
              border: OutlineInputBorder(),
              enabledBorder: OutlineInputBorder(
                borderRadius: sBorderRadius5,
                borderSide: BorderSide(width: 1, color: cBlack15),
              ),
              prefixIcon: Icon(widget.prefix, size: 18),
              suffixIcon: CupertinoButton(
                onPressed: () =>
                    _togglePinController.setTogglePassword(hiddenPassword),
                padding: sNoPadding,
                minSize: 5,
                child: Icon(
                    hiddenPassword ? LineIcons.lock : LineIcons.lockOpen,
                    size: 18,
                    color: cBlack50),
              ),
            ),
            obscureText: hiddenPassword,
          );
        },
      ),
    );
  }
}

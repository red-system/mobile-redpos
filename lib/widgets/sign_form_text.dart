import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:redpos/constants/color.dart';
import 'package:redpos/constants/style.dart';

class SignFormText extends StatelessWidget {
  const SignFormText({
    Key key,
    @required this.name,
    @required this.title,
    @required this.tipe,
    @required this.prefix,
    this.margin = 20,
  }) : super(key: key);

  final String name, title, tipe;
  final double margin;
  final prefix;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: margin),
      child: FormBuilderTextField(
        name: name,
        style: TextStyle(fontSize: 14),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(context,
              errorText: 'Kolom tidak boleh kosong.'),
        ]),
        decoration: InputDecoration(
          hintText: 'Input $title',
          hintStyle: TextStyle(fontSize: 14),
          isDense: true,
          contentPadding: sNoPadding,
          border: OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(
            borderRadius: sBorderRadius5,
            borderSide: BorderSide(width: 1, color: cBlack15),
          ),
          prefixIcon: Icon(prefix, size: 18),
        ),
      ),
    );
  }
}
